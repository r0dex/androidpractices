package ru.mirea.pushkarev.lesson4;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;

import ru.mirea.pushkarev.lesson4.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private boolean isPlaying = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.playlistTextView.setText("Мой номер по списку №22");

        binding.playPauseButton.setOnClickListener(v -> {
            if (isPlaying) {
                binding.playPauseButton.setImageResource(R.drawable.pause_button);
                isPlaying = false;
            } else {
                binding.playPauseButton.setImageResource(R.drawable.play_button);
                isPlaying = true;
            }
        });
    }
}