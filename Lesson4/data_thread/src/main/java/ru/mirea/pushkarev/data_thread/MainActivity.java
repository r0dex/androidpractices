package ru.mirea.pushkarev.data_thread;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.TimeUnit;

import ru.mirea.pushkarev.data_thread.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        final StringBuilder sequenceBuilder = new StringBuilder();

        final Runnable runn1 = new Runnable() {
            public void run() {
                binding.tvInfoState.setText("Поток: runn1");
                sequenceBuilder.append("runn1 ");
                binding.tvSequence.setText(sequenceBuilder.toString());
            }
        };
        final Runnable runn2 = new Runnable() {
            public void run() {
                binding.tvInfoState.setText("Поток: runn2");
                sequenceBuilder.append("runn2 ");
                binding.tvSequence.setText(sequenceBuilder.toString());
            }
        };
        final Runnable runn3 = new Runnable() {
            public void run() {
                binding.tvInfoState.setText("Поток: runn3");
                sequenceBuilder.append("runn3 ");
                sequenceBuilder.append("\n\nrunOnUiThread позволяет выполнять код в основном потоке, post добавляет код в очередь сообщений (также для выполнения в осн. потоке), а postDelayed нужен для запланированного выполнения кода с задержкой");
                binding.tvSequence.setText(sequenceBuilder.toString());
            }
        };
        Thread t = new Thread(new Runnable() {
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(2);
                    runOnUiThread(runn1);
                    TimeUnit.SECONDS.sleep(1);
                    new Handler(Looper.getMainLooper()).postDelayed(runn3, 2000);
                    new Handler(Looper.getMainLooper()).post(runn2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }
}
