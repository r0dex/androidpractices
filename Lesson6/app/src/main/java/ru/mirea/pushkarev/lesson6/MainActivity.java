package ru.mirea.pushkarev.lesson6;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import ru.mirea.pushkarev.lesson6.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        SharedPreferences sharedPref = getSharedPreferences("mirea_settings", Context.MODE_PRIVATE);

        binding.groupNumber.setText(sharedPref.getString("groupNumber", ""));
        binding.listNumber.setText(sharedPref.getString("listNumber", ""));
        binding.favoriteFilm.setText(sharedPref.getString("favoriteFilm", ""));

        binding.button.setOnClickListener(v -> {
            String groupNumber = binding.groupNumber.getText().toString();
            String listNumber = binding.listNumber.getText().toString();
            String favoriteFilm = binding.favoriteFilm.getText().toString();

            Toast.makeText(MainActivity.this, "Данные сохранены", Toast.LENGTH_SHORT).show();

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("groupNumber", groupNumber);
            editor.putString("listNumber", listNumber);
            editor.putString("favoriteFilm", favoriteFilm);
            editor.apply();
        });
    }
}
