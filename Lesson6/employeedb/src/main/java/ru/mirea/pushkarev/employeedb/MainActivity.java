package ru.mirea.pushkarev.employeedb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import ru.mirea.pushkarev.employeedb.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.addButton.setOnClickListener(v -> {
            String idString = binding.editTextId.getText().toString();
            String name = binding.editTextName.getText().toString();
            String superpower = binding.editTextSuperpower.getText().toString();

            if (idString.isEmpty() || name.isEmpty() || superpower.isEmpty()) {
                binding.infoTextView.setText("Пожалуйста, заполните все поля");
                return;
            }

            int id = Integer.parseInt(idString);

            Superhero superhero = new Superhero();
            superhero.id = id;
            superhero.name = name;
            superhero.superpower = superpower;

            App.getInstance().getDatabase().superheroDao().insert(superhero);
        });

        binding.getButton.setOnClickListener(v -> {
            String idString = binding.editTextId.getText().toString();

            if (idString.isEmpty()) {
                binding.infoTextView.setText("Введите ID супергероя");
                return;
            }

            int id = Integer.parseInt(idString);

            Superhero superhero = App.getInstance().getDatabase().superheroDao().getById(id);

            if (superhero != null) {
                String superheroInfo = "Имя: " + superhero.name + "\nСуперсила: " + superhero.superpower;
                binding.infoTextView.setText(superheroInfo);
            } else {
                binding.infoTextView.setText("Супергерой не найден");
            }
        });
    }
}
