package ru.mirea.pushkarev.employeedb;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface SuperheroDao {
    @Query("SELECT * FROM Superhero")
    List<Superhero> getAll();
    @Query("SELECT * FROM Superhero WHERE id = :id")
    Superhero getById(long id);
    @Insert
    void insert(Superhero superhero);
}