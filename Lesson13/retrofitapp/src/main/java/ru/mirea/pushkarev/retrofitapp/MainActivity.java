package ru.mirea.pushkarev.retrofitapp;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TodoAdapter todoAdapter;
    private ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        apiService = RetrofitClient.getClient().create(ApiService.class);

        List<Todo> todos = new ArrayList<>();
        todos.add(new Todo(1, 1, "Task 1", true, "https://www.carscoops.com/wp-content/uploads/2012/08/8e149e27-ferrari-f12berlinetta-825255b225255d.jpg"));
        todos.add(new Todo(1, 2, "Task 2", false, "https://i.pinimg.com/736x/d4/eb/3c/d4eb3cf5247c9bd36ad9039a79c50146.jpg"));
        todos.add(new Todo(1, 3, "Task 3", true, "https://www.avtorinok.ru/photo/pics/anteros/xtm-roadster/62750.jpg"));
        todos.add(new Todo(1, 4, "Task 4", false, "https://a.d-cd.net/2180cc5s-1920.jpg"));
        todos.add(new Todo(1, 5, "Task 5", true, "https://avatars.mds.yandex.net/get-mpic/5301034/img_id7060881085390753594.jpeg/orig"));

        todoAdapter = new TodoAdapter(this, todos);
        recyclerView.setAdapter(todoAdapter);
    }
}