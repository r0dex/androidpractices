package ru.mirea.pushkarev.retrofitapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.TodoViewHolder> {

    private LayoutInflater layoutInflater;
    private List<Todo> todos;
    private ApiService apiService;

    public TodoAdapter(Context context, List<Todo> todos) {
        this.layoutInflater = LayoutInflater.from(context);
        this.todos = todos;
        this.apiService = RetrofitClient.getClient().create(ApiService.class);
    }

    @NonNull
    @Override
    public TodoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_todo, parent, false);
        return new TodoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TodoViewHolder holder, int position) {
        Todo todo = todos.get(position);
        holder.textViewTitle.setText(todo.getTitle());
        holder.checkBoxCompleted.setChecked(todo.getCompleted());

        Picasso.get()
                .load(todo.getImageUrl()) // Здесь используем URL изображения
                .resize(100, 100)  // Ограничиваем размер изображения
                .centerCrop()
                .transform(new MonochromeTransformation())  // Применяем монохромный фильтр
                .into(holder.imageView);

        holder.imageView.setOnClickListener(v -> {
            showImageSettingsDialog(holder.itemView.getContext(), holder.imageView, todo); // Передаем todo
        });

        holder.checkBoxCompleted.setOnCheckedChangeListener((buttonView, isChecked) -> {
            todo.setCompleted(isChecked);

            // Отправляем запрос на обновление состояния задачи на сервере
            apiService.updateTodo(todo.getId(), todo).enqueue(new Callback<Todo>() {
                @Override
                public void onResponse(Call<Todo> call, Response<Todo> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(holder.itemView.getContext(), "Задача обновлена!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(holder.itemView.getContext(), "Ошибка обновления задачи", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Todo> call, Throwable t) {
                    Toast.makeText(holder.itemView.getContext(), "Ошибка: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    @Override
    public int getItemCount() {
        return todos.size();
    }

    public static class TodoViewHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle;
        CheckBox checkBoxCompleted;
        ImageView imageView;

        public TodoViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            checkBoxCompleted = itemView.findViewById(R.id.checkBoxCompleted);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }

    private void showImageSettingsDialog(Context context, ImageView imageView, Todo todo) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Настройки изображения");

        builder.setSingleChoiceItems(new String[]{"Монохром", "Обычное изображение"}, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    Picasso.get()
                            .load(todo.getImageUrl())
                            .resize(100, 100)
                            .centerCrop()
                            .transform(new MonochromeTransformation())  // Применяем монохромный фильтр
                            .into(imageView);
                } else {
                    Picasso.get()
                            .load(todo.getImageUrl())
                            .resize(100, 100)
                            .centerCrop()
                            .into(imageView);  // Обычное изображение
                }
            }
        });

        builder.setPositiveButton("OK", (dialog, which) -> dialog.dismiss());
        builder.setNegativeButton("Отмена", (dialog, which) -> dialog.dismiss());

        builder.show();
    }

    public static class MonochromeTransformation implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            Bitmap result = Bitmap.createBitmap(source.getWidth(), source.getHeight(), source.getConfig());

            Canvas canvas = new Canvas(result);
            Paint paint = new Paint();
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.setSaturation(0);  // Монохромный фильтр
            paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
            canvas.drawBitmap(source, 0, 0, paint);

            if (source != result) {
                source.recycle();
            }

            return result;
        }

        @Override
        public String key() {
            return "monochrome";
        }
    }
}