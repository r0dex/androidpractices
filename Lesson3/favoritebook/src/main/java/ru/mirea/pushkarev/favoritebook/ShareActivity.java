package ru.mirea.pushkarev.favoritebook;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ShareActivity extends AppCompatActivity {

    EditText editTextBook;
    TextView textViewBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        editTextBook = findViewById(R.id.editTextBook);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String bookName = extras.getString(MainActivity.KEY);
            textViewBook = findViewById(R.id.textViewBook);
            textViewBook.setText("Любимая книга разработчика – " + bookName);
        }
    }

    public void shareBook(View view) {
        String bookName = editTextBook.getText().toString();
        Intent data = new Intent();
        data.putExtra(MainActivity.USER_MESSAGE, bookName);
        setResult(Activity.RESULT_OK, data);
        finish();
    }
}