package ru.mirea.pushkarev.intentapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Bundle args = getIntent().getExtras();
        assert args != null;
        String message = args.getString("text");
        TextView textView = findViewById(R.id.textView);
        textView.setText(message);
    }
}