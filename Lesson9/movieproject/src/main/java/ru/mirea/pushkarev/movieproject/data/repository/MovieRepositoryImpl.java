package ru.mirea.pushkarev.movieproject.data.repository;

import android.content.Context;
import android.content.SharedPreferences;

import ru.mirea.pushkarev.movieproject.domain.models.Movie;
import ru.mirea.pushkarev.movieproject.domain.repository.MovieRepository;

public class MovieRepositoryImpl implements MovieRepository {
    private Context context;

    public MovieRepositoryImpl(Context context) {
        this.context = context;
    }

    @Override
    public boolean saveMovie(Movie movie) {
        SharedPreferences preferences = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("movie_id", 1);
        editor.putString("movie_name", movie.getName());
        return editor.commit();
    }

    @Override
    public Movie getMovie() {
        SharedPreferences preferences = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        String movieName = preferences.getString("movie_name", null);
        return new Movie(movieName);
    }
}
