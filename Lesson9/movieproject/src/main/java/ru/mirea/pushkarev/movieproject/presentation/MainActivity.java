package ru.mirea.pushkarev.movieproject.presentation;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import ru.mirea.pushkarev.movieproject.R;
import ru.mirea.pushkarev.movieproject.data.repository.MovieRepositoryImpl;
import ru.mirea.pushkarev.movieproject.domain.models.Movie;
import ru.mirea.pushkarev.movieproject.domain.repository.MovieRepository;
import ru.mirea.pushkarev.movieproject.domain.usecases.GetFavoriteFilmUseCase;
import ru.mirea.pushkarev.movieproject.domain.usecases.SaveMovieToFavoriteUseCase;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText text = findViewById(R.id.inputFilm);
        TextView textView = findViewById(R.id.showFilm);
        MovieRepository movieRepository = new MovieRepositoryImpl(this);

        findViewById(R.id.buttonSaveMovie).setOnClickListener(view -> {
            Boolean result = new SaveMovieToFavoriteUseCase(movieRepository).execute(new Movie(text.getText().toString()));
            // я поменял конструктор: теперь он берёт только имя фильма (потому что мы выводим только один любимый фильм)
            textView.setText(String.format("Save result %s", result));
        });

        findViewById(R.id.buttonGetMovie).setOnClickListener(view -> {
            Movie movie = new GetFavoriteFilmUseCase(movieRepository).execute();
            textView.setText(String.format("Favorite movie: %s", movie.getName()));
        });
    }

}