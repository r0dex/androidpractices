package ru.mirea.pushkarev.movieproject.domain.repository;

import ru.mirea.pushkarev.movieproject.domain.models.Movie;

public interface MovieRepository {
    public boolean saveMovie(Movie movie);
    public Movie getMovie();
}