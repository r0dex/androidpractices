package ru.mirea.pushkarev.domain.usecases;

import ru.mirea.pushkarev.domain.models.Currency;
import ru.mirea.pushkarev.domain.repository.CurrencyRepository;

public class GetCurrencyDetailsUseCase {
    private final CurrencyRepository repository;

    public GetCurrencyDetailsUseCase(CurrencyRepository repository) {
        this.repository = repository;
    }

    public Currency execute(String currencyCode) {
        return repository.getCurrencyDetails(currencyCode);
    }
}
