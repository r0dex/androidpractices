package ru.mirea.pushkarev.domain.usecases;

import ru.mirea.pushkarev.domain.models.Currency;
import ru.mirea.pushkarev.domain.repository.CurrencyRepository;
import java.util.List;

public class GetCurrencyListUseCase {
    private final CurrencyRepository repository;

    public GetCurrencyListUseCase(CurrencyRepository repository) {
        this.repository = repository;
    }

    public List<Currency> execute() {
        return repository.getCurrencyList();
    }
}
