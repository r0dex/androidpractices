package ru.mirea.pushkarev.domain.repository;

import ru.mirea.pushkarev.domain.models.Currency;
import java.util.List;

public interface CurrencyRepository {
    List<Currency> getCurrencyList();
    Currency getCurrencyDetails(String code);
}
