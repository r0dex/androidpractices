package ru.mirea.pushkarev.data.storage;

import ru.mirea.pushkarev.data.storage.models.Currency;
import java.util.List;

public interface CurrencyStorage {
    List<Currency> getAllCurrencies();
    Currency getCurrency(String code);
}
