package ru.mirea.pushkarev.data.repository;

import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.android.gms.tasks.Task;

import java.util.Objects;

import ru.mirea.pushkarev.domain.repository.UserRepository;

public class UserRepositoryImpl implements UserRepository {
    private final FirebaseAuth auth;

    public UserRepositoryImpl() {
        this.auth = FirebaseAuth.getInstance();
    }

    @Override
    public Task<FirebaseUser> signUp(String email, String password) {
        return auth.createUserWithEmailAndPassword(email, password)
                .continueWithTask(task -> {
                    if (task.isSuccessful()) {
                        return Tasks.forResult(auth.getCurrentUser());
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }

    @Override
    public Task<FirebaseUser> signIn(String email, String password) {
        return auth.signInWithEmailAndPassword(email, password)
                .continueWithTask(task -> {
                    if (task.isSuccessful()) {
                        return Tasks.forResult(auth.getCurrentUser());
                    } else {
                        throw Objects.requireNonNull(task.getException());
                    }
                });
    }

    @Override
    public void signOut() {
        auth.signOut();
    }

    @Override
    public FirebaseUser getCurrentUser() {
        return auth.getCurrentUser();
    }
}
