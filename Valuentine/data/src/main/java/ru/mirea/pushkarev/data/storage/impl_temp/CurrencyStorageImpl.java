package ru.mirea.pushkarev.data.storage.impl_temp;

import java.util.List;

import ru.mirea.pushkarev.data.storage.CurrencyStorage;
import ru.mirea.pushkarev.data.storage.network.NetworkApi; 
import ru.mirea.pushkarev.data.storage.models.Currency;
import ru.mirea.pushkarev.data.storage.room.AppDatabase;
import ru.mirea.pushkarev.data.storage.room.CurrencyDao;

public class CurrencyStorageImpl implements CurrencyStorage {

    private final NetworkApi networkApi;
    private final CurrencyDao currencyDao;

    public CurrencyStorageImpl(AppDatabase database) {
        this.networkApi = new NetworkApi(); 
        this.currencyDao = database.currencyDao(); 
    }

    @Override
    public List<Currency> getAllCurrencies() {
        List<Currency> currencies = currencyDao.getAllCurrencies();
        if (currencies.isEmpty()) {
            currencies = networkApi.fetchCurrencies();
            for (Currency currency : currencies) {
                currencyDao.insertCurrency(currency); 
            }
        }
        return currencies;
    }

    @Override
    public Currency getCurrency(String code) {
        Currency currency = currencyDao.getCurrencyByCode(code);
        if (currency == null) {
            for (Currency c : networkApi.fetchCurrencies()) {
                if (c.getCode().equalsIgnoreCase(code)) {
                    return c; 
                }
            }
        }
        return currency; 
    }
}
