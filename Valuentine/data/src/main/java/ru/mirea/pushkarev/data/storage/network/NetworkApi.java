package ru.mirea.pushkarev.data.storage.network;

import java.util.List;
import ru.mirea.pushkarev.data.storage.models.Currency;

public class NetworkApi {
    public List<Currency> fetchCurrencies() {
        return List.of(
                new Currency("USD", "Доллар США", 1.0),
                new Currency("EUR", "Евро", 0.85),
                new Currency("JPY", "Йена", 110.53)
        );
    }
}
