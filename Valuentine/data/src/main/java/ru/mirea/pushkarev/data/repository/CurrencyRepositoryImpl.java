package ru.mirea.pushkarev.data.repository;

import ru.mirea.pushkarev.data.storage.CurrencyStorage;
import ru.mirea.pushkarev.domain.repository.CurrencyRepository;

import java.util.ArrayList;
import java.util.List;

public class CurrencyRepositoryImpl implements CurrencyRepository {

    private CurrencyStorage currencyStorage;

    public CurrencyRepositoryImpl(CurrencyStorage currencyStorage) {
        this.currencyStorage = currencyStorage;
    }

    @Override
    public List<ru.mirea.pushkarev.domain.models.Currency> getCurrencyList() {
        List<ru.mirea.pushkarev.data.storage.models.Currency> storageCurrencies = currencyStorage.getAllCurrencies();
        List<ru.mirea.pushkarev.domain.models.Currency> domainCurrencies = new ArrayList<>();
        for (ru.mirea.pushkarev.data.storage.models.Currency storageCurrency : storageCurrencies) {
            domainCurrencies.add(new ru.mirea.pushkarev.domain.models.Currency(
                    storageCurrency.getCode(),
                    storageCurrency.getName(),
                    storageCurrency.getValue()
            ));
        }
        return domainCurrencies;
    }

    @Override
    public ru.mirea.pushkarev.domain.models.Currency getCurrencyDetails(String code) {
        ru.mirea.pushkarev.data.storage.models.Currency storageCurrency = currencyStorage.getCurrency(code);
        if (storageCurrency != null) {  
            return new ru.mirea.pushkarev.domain.models.Currency(
                    storageCurrency.getCode(),
                    storageCurrency.getName(),
                    storageCurrency.getValue()
            );
        }
        return null;
    }
}
