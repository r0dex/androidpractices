package ru.mirea.pushkarev.data.storage.room;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;

import ru.mirea.pushkarev.data.storage.models.Currency;

@Database(entities = {Currency.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract CurrencyDao currencyDao();

    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "currency_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}
