package ru.mirea.pushkarev.data.storage.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import ru.mirea.pushkarev.data.storage.models.Currency;

@Dao
public interface CurrencyDao {
    @Insert
    void insertCurrency(Currency currency);

    @Query("SELECT * FROM currencies")
    List<Currency> getAllCurrencies();

    @Query("SELECT * FROM currencies WHERE code = :currencyCode LIMIT 1")
    Currency getCurrencyByCode(String currencyCode);
}
