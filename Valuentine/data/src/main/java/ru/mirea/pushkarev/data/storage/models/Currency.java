package ru.mirea.pushkarev.data.storage.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "currencies")
public class Currency {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String code;
    private String name;
    private double value;

    @Ignore
    public Currency(String code, String name, double value) {
        this.code = code;
        this.name = name;
        this.value = value;
    }

    public Currency() {
        this.code = "";
        this.name = "";
        this.value = 0.0;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
