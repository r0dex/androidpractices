package ru.mirea.pushkarev.valuentine.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.firebase.auth.FirebaseUser;
import ru.mirea.pushkarev.data.repository.UserRepositoryImpl;
import ru.mirea.pushkarev.domain.usecases.LoginUserUseCase;
import ru.mirea.pushkarev.domain.usecases.GetUserProfileUseCase;
import ru.mirea.pushkarev.valuentine.R;

public class LoginActivity extends AppCompatActivity {
    private EditText emailInput, passwordInput;
    private Button loginButton;
    private TextView registerLink, guestLoginLink;
    private LoginUserUseCase loginUserUseCase;
    private GetUserProfileUseCase getUserProfileUseCase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailInput = findViewById(R.id.email_input);
        passwordInput = findViewById(R.id.password_input);
        loginButton = findViewById(R.id.login_button);
        registerLink = findViewById(R.id.register_link);
        guestLoginLink = findViewById(R.id.guest_login_link);

        loginUserUseCase = new LoginUserUseCase(new UserRepositoryImpl());
        getUserProfileUseCase = new GetUserProfileUseCase(new UserRepositoryImpl());

        loginButton.setOnClickListener(view -> {
            String email = emailInput.getText().toString();
            String password = passwordInput.getText().toString();

            loginUserUseCase.execute(email, password).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    FirebaseUser user = task.getResult();
                    Toast.makeText(this, "E-mail: " + user.getEmail(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish(); 
                } else {
                    Toast.makeText(this, "Ошибка входа: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        });

        registerLink.setOnClickListener(view -> {
            startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
        });

        guestLoginLink.setOnClickListener(view -> {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra("is_guest", true);
            startActivity(intent);
            finish();
        });

    }
}
