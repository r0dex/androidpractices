package ru.mirea.pushkarev.valuentine.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseUser;

import ru.mirea.pushkarev.data.repository.UserRepositoryImpl;
import ru.mirea.pushkarev.data.storage.impl_temp.CurrencyStorageImpl;
import ru.mirea.pushkarev.data.repository.CurrencyRepositoryImpl;
import ru.mirea.pushkarev.data.storage.room.AppDatabase;
import ru.mirea.pushkarev.domain.models.Currency;
import ru.mirea.pushkarev.domain.usecases.GetCurrencyListUseCase;
import ru.mirea.pushkarev.domain.usecases.GetUserProfileUseCase;
import ru.mirea.pushkarev.valuentine.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private GetCurrencyListUseCase getCurrencyListUseCase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        GetUserProfileUseCase getUserProfileUseCase = new GetUserProfileUseCase(new UserRepositoryImpl());
        FirebaseUser currentUser = getUserProfileUseCase.execute();

        if (currentUser == null) {
            boolean isGuest = getIntent().getBooleanExtra("is_guest", false);
            if (isGuest) {
                setContentView(R.layout.activity_main);
                initializeCurrencyList();
                return; 
            }

            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish(); 
            return; 
        }
        
        setContentView(R.layout.activity_main);
        initializeCurrencyList();
    }

    private void initializeCurrencyList() {
        listView = findViewById(R.id.currency_list);
        AppDatabase database = AppDatabase.getInstance(getApplicationContext());
        CurrencyStorageImpl currencyStorage = new CurrencyStorageImpl(database);
        CurrencyRepositoryImpl currencyRepository = new CurrencyRepositoryImpl(currencyStorage);
        getCurrencyListUseCase = new GetCurrencyListUseCase(currencyRepository);

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            List<Currency> currencies = getCurrencyListUseCase.execute();
            runOnUiThread(() -> {
                if (currencies != null) {
                    List<String> currencyNames = new ArrayList<>();
                    for (Currency currency : currencies) {
                        currencyNames.add(currency.getName() + " (" + currency.getCode() + ")");
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,
                            android.R.layout.simple_list_item_1, currencyNames);
                    listView.setAdapter(adapter);
                    
                    listView.setOnItemClickListener((parent, view, position, id) -> {
                        Currency selectedCurrency = currencies.get(position);
                        Intent intent = new Intent(MainActivity.this, CurrencyDetailsActivity.class);
                        intent.putExtra("currency_code", selectedCurrency.getCode()); 
                        startActivity(intent); 
                    });
                }
            });
        });
    }
}
