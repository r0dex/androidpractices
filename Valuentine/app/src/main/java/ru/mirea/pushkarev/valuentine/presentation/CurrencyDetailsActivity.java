package ru.mirea.pushkarev.valuentine.presentation;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.mirea.pushkarev.valuentine.R;
import ru.mirea.pushkarev.data.storage.impl_temp.CurrencyStorageImpl;
import ru.mirea.pushkarev.data.repository.CurrencyRepositoryImpl;
import ru.mirea.pushkarev.domain.models.Currency;
import ru.mirea.pushkarev.domain.usecases.GetCurrencyDetailsUseCase;
import ru.mirea.pushkarev.data.storage.room.AppDatabase;

public class CurrencyDetailsActivity extends AppCompatActivity {
    private TextView currencyDetails;
    private GetCurrencyDetailsUseCase getCurrencyDetailsUseCase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_details);

        currencyDetails = findViewById(R.id.currency_details);
        String currencyCode = getIntent().getStringExtra("currency_code");

        if (currencyCode == null) {
            currencyDetails.setText("Код валюты не передан");
            return;
        }

        ExecutorService executor = Executors.newSingleThreadExecutor();
        AppDatabase appDatabase = AppDatabase.getInstance(this);
        CurrencyStorageImpl currencyStorage = new CurrencyStorageImpl(appDatabase);
        CurrencyRepositoryImpl currencyRepository = new CurrencyRepositoryImpl(currencyStorage);
        getCurrencyDetailsUseCase = new GetCurrencyDetailsUseCase(currencyRepository);

        executor.execute(() -> {
            Currency currency = getCurrencyDetailsUseCase.execute(currencyCode);
            runOnUiThread(() -> {
                if (currency != null) {
                    currencyDetails.setText("Название: " + currency.getName() + "\nКурс: " + currency.getRate());
                } else {
                    currencyDetails.setText("Валюта не найдена");
                }
            });
        });
    }
}
