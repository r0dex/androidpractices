package ru.mirea.pushkarev.valuentine.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import ru.mirea.pushkarev.data.repository.UserRepositoryImpl;
import ru.mirea.pushkarev.domain.usecases.RegisterUserUseCase;
import ru.mirea.pushkarev.valuentine.R;

public class RegistrationActivity extends AppCompatActivity {
    private EditText emailInput, passwordInput;
    private TextView loginLink, guestLoginLink;
    private Button registerButton;
    private RegisterUserUseCase registerUserUseCase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        emailInput = findViewById(R.id.email_input);
        passwordInput = findViewById(R.id.password_input);
        loginLink = findViewById(R.id.login_link);
        guestLoginLink = findViewById(R.id.guest_login_link);
        registerButton = findViewById(R.id.register_button);

        registerUserUseCase = new RegisterUserUseCase(new UserRepositoryImpl());

        registerButton.setOnClickListener(view -> {
            String email = emailInput.getText().toString();
            String password = passwordInput.getText().toString();

            registerUserUseCase.execute(email, password).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Toast.makeText(this, "Регистрация успешна!", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(this, "Ошибка регистрации: " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        });

        loginLink.setOnClickListener(view -> {
            Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
            startActivity(intent);
        });

        guestLoginLink.setOnClickListener(view -> {
            startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
            finish();
        });
    }
}
