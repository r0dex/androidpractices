package ru.mirea.pushkarev.mireaproject.ui.establishments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import ru.mirea.pushkarev.mireaproject.R;
import ru.mirea.pushkarev.mireaproject.databinding.FragmentEstablishmentsBinding;

public class EstablishmentsFragment extends Fragment {

    private FragmentEstablishmentsBinding binding;
    private Context mContext;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentEstablishmentsBinding.inflate(inflater, container, false);
        mContext = getContext();
        loadPlacesFromJson();
        return binding.getRoot();
    }

    private void loadPlacesFromJson() {
        try {
            JSONArray placesArray = new JSONArray(loadJsonFromAsset());
            for (int i = 0; i < placesArray.length(); i++) {
                JSONObject placeObject = placesArray.getJSONObject(i);
                final String name = placeObject.getString("name");
                final String description = placeObject.getString("description");
                final double latitude = placeObject.getDouble("latitude");
                final double longitude = placeObject.getDouble("longitude");
                final String address = placeObject.getString("address");
                final String img = placeObject.getString("img");

                @SuppressLint("DiscouragedApi") int imageResourceId = getResources().getIdentifier(img, "drawable", getActivity().getPackageName());

                addPlaceCard(name, description, latitude, longitude, address, imageResourceId);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addPlaceCard(final String name, final String description, final double latitude, final double longitude, final String address, int imageResourceId) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View cardView = inflater.inflate(R.layout.item_establishments, binding.placesContainer, false);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MapActivity.class);

                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);
                intent.putExtra("address", address);
                intent.putExtra("description", description);

                mContext.startActivity(intent);
            }
        });

        binding.placesContainer.addView(cardView);

        // Настройка данных карточки
        TextView placeNameTextView = cardView.findViewById(R.id.placeName);
        TextView placeDescriptionTextView = cardView.findViewById(R.id.placeDescription);
        ImageView placeImage = cardView.findViewById(R.id.placeImage);
        placeNameTextView.setText(name);
        placeDescriptionTextView.setText(description);
        Drawable imageDrawable = getResources().getDrawable(imageResourceId, mContext.getTheme());
        placeImage.setImageDrawable(imageDrawable);

    }

    private String loadJsonFromAsset() {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream inputStream = mContext.getAssets().open("places.json");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            bufferedReader.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return stringBuilder.toString();
    }
}
