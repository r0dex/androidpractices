package ru.mirea.pushkarev.mireaproject.ui.stepcounter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import ru.mirea.pushkarev.mireaproject.databinding.FragmentStepCounterBinding;

public class StepCounterFragment extends Fragment implements SensorEventListener {

    private static final int PERMISSION_REQUEST_CODE = 100;

    private SensorManager sensorManager;
    private Sensor stepSensor;
    private TextView stepCountTextView;
    private Button goalButton;
    private Button resetButton;
    private EditText stepGoalEditText;
    private CardView cardView;
    private int offset = 0;
    private int goalSteps = -1;

    private FragmentStepCounterBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentStepCounterBinding.inflate(inflater, container, false);
        View rootView = binding.getRoot();

        sensorManager = (SensorManager) requireActivity().getSystemService(Context.SENSOR_SERVICE);
        stepSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        stepCountTextView = binding.stepCountText;
        goalButton = binding.goalButton;
        stepGoalEditText = binding.stepGoalEditText;
        cardView = binding.cardView;
        resetButton = binding.resetButton;

        if (stepSensor == null) {
            stepCountTextView.setText("Шагомер не поддерживается на этом устройстве");
        } else {
            checkPermissions();
        }

        goalButton.setOnClickListener(v -> {
            String goalText = stepGoalEditText.getText().toString();
            if (TextUtils.isEmpty(goalText)) {
                Toast.makeText(requireContext(), "Введите цель по шагам", Toast.LENGTH_SHORT).show();
                return;
            }
            goalSteps = Integer.parseInt(goalText);
            Toast.makeText(requireContext(), "Цель установлена: " + goalSteps + " шагов", Toast.LENGTH_SHORT).show();
            offset = 0;
            registerSensorListener();
        });

        resetButton.setOnClickListener(v -> {
            offset = 0;
            updateStepCountText(0);
        });

        return rootView;
    }

    private void checkPermissions() {
        if (ActivityCompat.checkSelfPermission(requireContext(), android.Manifest.permission.ACTIVITY_RECOGNITION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.ACTIVITY_RECOGNITION}, PERMISSION_REQUEST_CODE);
        } else {
            registerSensorListener();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                registerSensorListener();
            } else {
                Toast.makeText(requireContext(), "Необходимо разрешение на получение данных о физической активности", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void registerSensorListener() {
        if (stepSensor != null) {
            sensorManager.registerListener(this, stepSensor, SensorManager.SENSOR_DELAY_UI);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER && goalSteps != -1) {
            if (offset == 0) {
                offset = (int) event.values[0];
            }
            int steps = (int) event.values[0] - offset;
            updateStepCountText(steps);

            if (steps >= goalSteps) {
                cardView.setBackgroundColor(getResources().getColor(android.R.color.holo_green_light));
            } else {
                cardView.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private void updateStepCountText(int steps) {
        stepCountTextView.setText("Шаги: " + steps);
    }
}
