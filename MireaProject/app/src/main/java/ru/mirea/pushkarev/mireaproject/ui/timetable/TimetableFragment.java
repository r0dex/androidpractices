package ru.mirea.pushkarev.mireaproject.ui.timetable;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import ru.mirea.pushkarev.mireaproject.R;

public class TimetableFragment extends Fragment {
    private static final String TAG = TimetableFragment.class.getSimpleName();
    private LinearLayout timetableContainer;
    private TimetableService timetableService;
    private boolean isBound = false;

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            TimetableService.LocalBinder binder = (TimetableService.LocalBinder) service;
            timetableService = binder.getService();
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(requireContext(), TimetableService.class);
        requireContext().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isBound) {
            requireContext().unbindService(serviceConnection);
            isBound = false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_timetable, container, false);

        timetableContainer = root.findViewById(R.id.timetableContainer);
        Button updateButton = root.findViewById(R.id.updateButton);

        updateButton.setOnClickListener(v -> fetchTimetableData());

        return root;
    }

    private final Handler mainHandler = new Handler(Looper.getMainLooper());

    private void fetchTimetableData() {
        timetableService.fetchTimetableData(new TimetableService.TimetableDataCallback() {
            @Override
            public void onSuccess(JSONArray scheduleArray) {
                requireActivity().runOnUiThread(() -> updateTimetableUI(scheduleArray));
            }

            @Override
            public void onFailure(String errorMessage) {
                showToast("Ошибка при извлечении данных: " + errorMessage);
            }
        });
    }

    private void showToast(final String message) {
        mainHandler.post(() -> Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show());
    }

    @SuppressLint("RestrictedApi")
    private void updateTimetableUI(JSONArray scheduleArray) {
        if (scheduleArray == null || scheduleArray.length() == 0) {
            Toast.makeText(requireContext(), "Нет данных о расписании", Toast.LENGTH_SHORT).show();
            return;
        }
        timetableContainer.removeAllViews();

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+03:00"));
            String currentDateTime = dateFormat.format(new Date());
            TextView currentDateTextView = requireView().findViewById(R.id.currentDateTextView);
            currentDateTextView.setText(currentDateTime);

            for (int i = 0; i < scheduleArray.length(); i++) {
                JSONObject scheduleObject = scheduleArray.getJSONObject(i);
                JSONObject threadObject = scheduleObject.getJSONObject("thread");
                String trainNumber = threadObject.getString("number");
                String departureDateTime = scheduleObject.getString("departure");
                String time = departureDateTime.substring(11, 16);
                String title = threadObject.getString("title");
                String stops = scheduleObject.optString("stops", "").isEmpty() ? "нет" : scheduleObject.getString("stops");

                View cardView = LayoutInflater.from(requireContext()).inflate(R.layout.item_timetable, timetableContainer, false);

                TextView trainNumberTextView = cardView.findViewById(R.id.textTrainNumber);
                TextView departureTimeTextView = cardView.findViewById(R.id.textTime);
                TextView directionTextView = cardView.findViewById(R.id.textDirection);
                TextView stopsTextView = cardView.findViewById(R.id.textStops);

                trainNumberTextView.setText("Поезд " + trainNumber);
                departureTimeTextView.setText(time);
                directionTextView.setText(title);
                stopsTextView.setText("Остановки: " + stops);

                timetableContainer.addView(cardView);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Ошибка парсинга расписания: " + e.getMessage());
            Toast.makeText(requireContext(), "Ошибка парсинга расписания", Toast.LENGTH_SHORT).show();
        }
    }
}
