package ru.mirea.pushkarev.mireaproject.ui.compressor;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;

import androidx.exifinterface.media.ExifInterface;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.mirea.pushkarev.mireaproject.databinding.FragmentCompressorBinding;

public class CompressorFragment extends Fragment {

    private FragmentCompressorBinding binding;
    private Uri imageUri;
    private static final int REQUEST_IMAGE_PICK = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCompressorBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String compressionText = "Процент сжатия: " + progress + "%";
                binding.progressTextView.setText(compressionText);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        binding.imageView.setOnClickListener(v -> openImagePicker());
        binding.chooseImageButton.setOnClickListener(v -> openImagePicker());
        binding.fabCompress.setOnClickListener(v -> showCompressionConfirmationDialog());
    }

    private void openImagePicker() {
        Intent pickPhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if (pickPhotoIntent.resolveActivity(requireActivity().getPackageManager()) != null) {
            startActivityForResult(pickPhotoIntent, REQUEST_IMAGE_PICK);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == requireActivity().RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_PICK) {
                imageUri = data.getData();
                binding.imageView.setImageURI(imageUri);
            }
        }
    }

    private void showCompressionConfirmationDialog() {
        if (imageUri == null) {
            Toast.makeText(requireContext(), "Сначала выберите изображение", Toast.LENGTH_SHORT).show();
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setMessage("Вы хотите сжать изображение на " + binding.seekBar.getProgress() + "%?")
                .setCancelable(false)
                .setPositiveButton("Да", (dialog, id) -> compressAndSaveImage())
                .setNegativeButton("Нет", (dialog, id) -> dialog.dismiss());
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void compressAndSaveImage() {
        new ImageCompressionTask().execute(imageUri);
    }

    public class ImageCompressionTask extends AsyncTask<Uri, Void, File> {
        @Override
        protected File doInBackground(Uri... uris) {
            Uri imageUri = uris[0];
            Bitmap bitmap;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(requireActivity().getContentResolver(), imageUri);

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                android.database.Cursor cursor = requireActivity().getContentResolver().query(imageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                ExifInterface exif = new ExifInterface(picturePath);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                Bitmap rotatedBitmap = rotateBitmap(bitmap, orientation);

                File picturesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                File compressedDir = new File(picturesDir, "compressed");
                if (!compressedDir.exists()) {
                    compressedDir.mkdirs();
                }
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                String fileName = "compressed_image_" + timeStamp + ".jpg";

                File file = new File(compressedDir, fileName);

                FileOutputStream out = new FileOutputStream(file);
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, binding.seekBar.getProgress(), out);
                out.flush();
                out.close();

                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                mediaScanIntent.setData(Uri.fromFile(file));
                requireContext().sendBroadcast(mediaScanIntent);

                return file;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            if (file != null) {
                Toast.makeText(requireContext(), "Изображение сохранено в альбоме 'compressed'", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(requireContext(), "Ошибка при сжатии и сохранении изображения", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private Bitmap rotateBitmap(Bitmap bitmap, int orientation) {
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(bitmap, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(bitmap, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(bitmap, 270);
            default:
                return bitmap;
        }
    }

    private Bitmap rotateImage(Bitmap source, float angle) {
        android.graphics.Matrix matrix = new android.graphics.Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
}
