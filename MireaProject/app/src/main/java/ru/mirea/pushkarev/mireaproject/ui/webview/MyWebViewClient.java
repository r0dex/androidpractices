package ru.mirea.pushkarev.mireaproject.ui.webview;

import android.app.Activity;
import android.webkit.WebViewClient;

public class MyWebViewClient extends WebViewClient {

    private WebViewFragment activity = null;

    public MyWebViewClient(WebViewFragment activity) {
        this.activity = activity;
    }
}