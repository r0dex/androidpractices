package ru.mirea.pushkarev.mireaproject.ui.joke;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import ru.mirea.pushkarev.mireaproject.databinding.FragmentJokeBinding;

public class JokeFragment extends Fragment {

    private FragmentJokeBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentJokeBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        binding.buttonGetJoke.setOnClickListener(v -> new FetchJokeTask().execute());

        return view;
    }

    private class FetchJokeTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL("https://v2.jokeapi.dev/joke/Programming?type=twopart");
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();

                StringBuilder response = new StringBuilder();
                int data;
                while ((data = in.read()) != -1) {
                    response.append((char) data);
                }
                return response.toString();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String setup = jsonObject.getString("setup");
                    String delivery = jsonObject.getString("delivery");
                    binding.setupTextView.setText(setup);
                    binding.deliveryTextView.setText(delivery);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(requireContext(), "Ошибка при парсинге шутки", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(requireContext(), "Ошибка запроса", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
