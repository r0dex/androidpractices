package ru.mirea.pushkarev.mireaproject.ui.noisemeter;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.io.File;
import java.io.IOException;

import ru.mirea.pushkarev.mireaproject.R;
import ru.mirea.pushkarev.mireaproject.databinding.FragmentNoiseMeterBinding;

public class NoiseMeterFragment extends Fragment {

    private FragmentNoiseMeterBinding binding;
    private static final int REQUEST_CODE_PERMISSION = 200;
    private boolean isWork;
    private MediaRecorder recorder = null;
    String recordFilePath = null;

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentNoiseMeterBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        recordFilePath = (new File(inflater.getContext().getExternalFilesDir(Environment.DIRECTORY_MUSIC),
                "/audiorecordtest.3gp")).getAbsolutePath();

        int audioRecordPermissionStatus = ContextCompat.checkSelfPermission(inflater.getContext(),
                android.Manifest.permission.RECORD_AUDIO);
        int storagePermissionStatus = ContextCompat.checkSelfPermission(inflater.getContext(), android.Manifest.permission.
                WRITE_EXTERNAL_STORAGE);
        if (audioRecordPermissionStatus == PackageManager.PERMISSION_GRANTED && storagePermissionStatus
                == PackageManager.PERMISSION_GRANTED) {
            isWork = true;
        } else {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{android.Manifest.permission.RECORD_AUDIO,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION);
        }

        startRecording();
        binding.button.setOnClickListener(v -> {
            double amplitude = recorder.getMaxAmplitude();
            double db = 20 * Math.log10(amplitude / 32767.0) + 72.24693; //я получил это значение опытным путём, запустив прогу на виртуальном девайсе
            binding.noiseLevelValueTextView.setText(Double.toString(Math.round(db)) + " db");
            binding.noiseLevelValueTextView.setTextColor(getResources().getColor(chooseColor(db)));
            binding.noiseLevelTextView.setText("Уровень шума: " + chooseCategory(db));
            binding.noiseLevelTextView.setTextColor(getResources().getColor(chooseColor(db)));
        });

        return root;
    }

    private String chooseCategory(double db) {
        if (db < 45) {
            return "тихий";
        } else if (db < 65) {
            return "нормальный";
        } else if (db < 85) {
            return "громкий";
        } else if (db < 105) {
            return "очень громкий";
        } else return "оглушительный";
    }

    private int chooseColor(double db) {
        if (db < 45) {
            return R.color.colorQuiet;
        } else if (db < 65) {
            return R.color.colorNormal;
        } else if (db < 85) {
            return R.color.colorLoud;
        } else if (db < 105) {
            return R.color.colorVeryLoud;
        } else return R.color.colorExtreme;
    }

    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setOutputFile(recordFilePath);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e("TAG", "prepare() failed");
        }
        recorder.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION:
                isWork = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
    }
}