package ru.mirea.pushkarev.mireaproject.ui.profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import ru.mirea.pushkarev.mireaproject.databinding.FragmentProfileBinding;

public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;
    private SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentProfileBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        sharedPreferences = requireContext().getSharedPreferences("profileData", Context.MODE_PRIVATE);

        binding.firstName.setText(sharedPreferences.getString("firstName", ""));
        binding.lastName.setText(sharedPreferences.getString("lastName", ""));
        binding.dateOfBirth.setText(sharedPreferences.getString("dateOfBirth", ""));
        binding.emailAddress.setText(sharedPreferences.getString("emailAddress", ""));

        binding.saveDataButton.setOnClickListener(v -> saveProfileData());

        if (sharedPreferences.contains("firstName")) {
            binding.saveDataButton.setText("Обновить данные");
        } else {
            binding.saveDataButton.setText("Сохранить данные");
        }
    }

    private void saveProfileData() {
        String firstName = binding.firstName.getText().toString().trim();
        String lastName = binding.lastName.getText().toString().trim();
        String dateOfBirth = binding.dateOfBirth.getText().toString().trim();
        String emailAddress = binding.emailAddress.getText().toString().trim();

        if (firstName.isEmpty() || lastName.isEmpty() || dateOfBirth.isEmpty() || emailAddress.isEmpty()) {
            Toast.makeText(requireContext(), "Пожалуйста, заполните все поля", Toast.LENGTH_SHORT).show();
            return;
        }

        if (sharedPreferences.contains("firstName")) {
            Toast.makeText(requireContext(), "Данные обновлены", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(requireContext(), "Данные сохранены", Toast.LENGTH_SHORT).show();
            binding.saveDataButton.setText("Обновить данные");
        }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("firstName", firstName);
        editor.putString("lastName", lastName);
        editor.putString("dateOfBirth", dateOfBirth);
        editor.putString("emailAddress", emailAddress);
        editor.apply();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
