package ru.mirea.pushkarev.mireaproject.ui.timetable;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class TimetableService extends Service {
    private static final String TAG = TimetableService.class.getSimpleName();

    private final IBinder binder = new LocalBinder();

    public class LocalBinder extends Binder {
        TimetableService getService() {
            return TimetableService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void fetchTimetableData(TimetableDataCallback callback) {
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        String URL = "https://api.rasp.yandex.net/v3.0/schedule/?apikey=8c98851f-92df-44ce-8c1a-288883351307&station=s9601843&transport_types=suburban&direction=all&date=" + currentDate;

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(URL)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "Ошибка при извлечении: " + e.getMessage());
                callback.onFailure(e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.e(TAG, "Ошибка при извлечении данных: " + response.message());
                    callback.onFailure(response.message());
                    return;
                }

                try {
                    String responseData = response.body().string();
                    Log.d(TAG, "Response data: " + responseData);
                    JSONObject timetableObject = new JSONObject(responseData);
                    JSONArray timetableArray = timetableObject.getJSONArray("schedule");
                    callback.onSuccess(timetableArray);
                } catch (JSONException e) {
                    Log.e(TAG, "Ошибка парсинга расписания: " + e.getMessage());
                    callback.onFailure("Ошибка парсинга расписания");
                }
            }
        });
    }

    public interface TimetableDataCallback {
        void onSuccess(JSONArray timetableData);
        void onFailure(String errorMessage);
    }
}
