package ru.mirea.pushkarev.mireaproject.ui.MonochromeFilter;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.mirea.pushkarev.mireaproject.databinding.FragmentMonochromeFilterBinding;

public class MonochromeFilterFragment extends Fragment {
    private static final int REQUEST_CODE_PERMISSION = 100;
    private Uri imageUri;
    private FragmentMonochromeFilterBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentMonochromeFilterBinding.inflate(inflater, container, false);

        int cameraPermissionStatus = ContextCompat.checkSelfPermission(inflater.getContext(), android.Manifest.permission.CAMERA);
        int storagePermissionStatus = ContextCompat.checkSelfPermission(inflater.getContext(), WRITE_EXTERNAL_STORAGE);
        if (cameraPermissionStatus != PackageManager.PERMISSION_GRANTED || storagePermissionStatus != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.CAMERA, WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION);
        }

        ActivityResultCallback<ActivityResult> callback = result -> {
            if (result.getResultCode() == Activity.RESULT_OK) {
                Bitmap monochromeBitmap = convertToMonochrome(imageUri);
                binding.imageView1.setImageBitmap(monochromeBitmap);
            }
        };

        ActivityResultLauncher<Intent> cameraActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                callback);

        binding.imageView1.setOnClickListener(v -> {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            try {
                File photoFile = createImageFile();
                String authorities = inflater.getContext().getApplicationContext().getPackageName() + ".fileprovider";
                imageUri = FileProvider.getUriForFile(inflater.getContext(), authorities, photoFile);

                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                cameraActivityResultLauncher.launch(cameraIntent);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        return binding.getRoot();
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName, ".jpg", storageDirectory);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
        }
    }

    private Bitmap convertToMonochrome(Uri imageUri) {
        try {
            Bitmap originalBitmap = BitmapFactory.decodeStream(requireContext().getContentResolver().openInputStream(imageUri));

            Bitmap monochromeBitmap = Bitmap.createBitmap(originalBitmap.getWidth(), originalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
            int[] pixels = new int[originalBitmap.getWidth() * originalBitmap.getHeight()];
            originalBitmap.getPixels(pixels, 0, originalBitmap.getWidth(), 0, 0, originalBitmap.getWidth(), originalBitmap.getHeight());
            for (int i = 0; i < pixels.length; i++) {
                int pixel = pixels[i];
                int gray = (int) (0.2989 * ((pixel >> 16) & 0xFF) + 0.5870 * ((pixel >> 8) & 0xFF) + 0.1140 * (pixel & 0xFF));
                pixels[i] = 0xFF000000 | (gray << 16) | (gray << 8) | gray;
            }
            monochromeBitmap.setPixels(pixels, 0, originalBitmap.getWidth(), 0, 0, originalBitmap.getWidth(), originalBitmap.getHeight());

            return monochromeBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
