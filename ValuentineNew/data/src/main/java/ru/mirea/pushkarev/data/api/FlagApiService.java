package ru.mirea.pushkarev.data.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface FlagApiService {
    @GET("{countryCode}/flat/64.png")  // Относительный путь для флага
    Call<ResponseBody> getFlagUrl(@Path("countryCode") String countryCode);
}
