package ru.mirea.pushkarev.data.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitFlagClient {

    private static final String BASE_URL = "https://flagsapi.com/"; // Базовый URL для API флагов
    private static Retrofit retrofit;

    public static FlagApiService getFlagApiService() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit.create(FlagApiService.class);
    }
}
