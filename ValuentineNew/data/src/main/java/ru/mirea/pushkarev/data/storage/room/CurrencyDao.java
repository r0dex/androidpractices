package ru.mirea.pushkarev.data.storage.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import ru.mirea.pushkarev.data.storage.models.Currency;

@Dao
public interface CurrencyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCurrency(Currency currency);

    @Query("SELECT * FROM currencies WHERE code = :currencyCode LIMIT 1")
    Currency getCurrency(String currencyCode);

    @Query("SELECT * FROM currencies")
    LiveData<List<Currency>> getAllCurrencies(); 
}


