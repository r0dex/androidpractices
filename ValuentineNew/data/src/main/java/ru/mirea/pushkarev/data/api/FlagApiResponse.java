package ru.mirea.pushkarev.data.api;

public class FlagApiResponse {
    private String flags; // предполагается, что API возвращает ссылку на флаг в поле "flags"

    public String getFlags() {
        return flags;
    }

    public void setFlags(String flags) {
        this.flags = flags;
    }
}
