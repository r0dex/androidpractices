package ru.mirea.pushkarev.data.api;

import retrofit2.Call;
import retrofit2.http.GET;
import java.util.Map;

public interface CurrencyApiService {
    @GET("latest/RUB")
    Call<CurrencyApiResponse> getExchangeRates();
}
