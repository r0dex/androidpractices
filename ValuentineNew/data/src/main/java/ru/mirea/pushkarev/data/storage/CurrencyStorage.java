package ru.mirea.pushkarev.data.storage;

import androidx.lifecycle.LiveData;

import java.util.List;

import ru.mirea.pushkarev.data.storage.models.Currency;

public interface CurrencyStorage {
    LiveData<List<Currency>> getDbCurrencies(); // Получение валют из базы данных
    LiveData<List<Currency>> getApiCurrencies(); // Получение валют из API
    LiveData<Currency> getCurrencyLiveData(String code);  // Получение информации о валюте по коду
}
