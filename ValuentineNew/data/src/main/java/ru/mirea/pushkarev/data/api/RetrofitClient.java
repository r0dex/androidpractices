package ru.mirea.pushkarev.data.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static final String BASE_URL = "https://open.er-api.com/v6/";
    private static final String FLAG_BASE_URL = "https://flagsapi.com/";

    private static Retrofit retrofit;
    private static Retrofit flagRetrofit;

    public static CurrencyApiService getCurrencyApiService() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit.create(CurrencyApiService.class);
    }

    public static FlagApiService getFlagApiService() {
        if (flagRetrofit == null) {
            flagRetrofit = new Retrofit.Builder()
                    .baseUrl(FLAG_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return flagRetrofit.create(FlagApiService.class);
    }
}
