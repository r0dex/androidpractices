package ru.mirea.pushkarev.data.storage.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "currencies")
public class Currency {

    @PrimaryKey
    @NonNull
    private String code;
    private double rate;
    private String name;
    private String description;
    private String flagUrl; // Поле для URL флага

    public Currency(@NonNull String code, double rate, String name, String description, String flagUrl) {
        this.code = code;
        this.rate = rate;
        this.name = name;
        this.description = description;
        this.flagUrl = flagUrl; // Инициализация флага
    }

    @NonNull
    public String getCode() {
        return code;
    }

    public void setCode(@NonNull String code) {
        this.code = code;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFlagUrl() {
        return flagUrl; // Геттер для флага
    }

    public void setFlagUrl(String flagUrl) {
        this.flagUrl = flagUrl; // Сеттер для флага
    }
}
