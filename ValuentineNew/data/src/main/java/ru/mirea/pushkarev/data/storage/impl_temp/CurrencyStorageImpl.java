package ru.mirea.pushkarev.data.storage.impl_temp;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.mirea.pushkarev.data.storage.CurrencyStorage;
import ru.mirea.pushkarev.data.storage.models.Currency;
import ru.mirea.pushkarev.data.api.CurrencyApiService;
import ru.mirea.pushkarev.data.api.CurrencyApiResponse;
import ru.mirea.pushkarev.data.api.FlagApiService;
import ru.mirea.pushkarev.data.storage.room.CurrencyDatabase;
import ru.mirea.pushkarev.data.storage.room.CurrencyDao;

public class CurrencyStorageImpl implements CurrencyStorage {

    private final CurrencyApiService currencyApiService;
    private final FlagApiService flagApiService;
    private final CurrencyDao currencyDao;
    private final MutableLiveData<List<Currency>> apiCurrenciesLiveData = new MutableLiveData<>();
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private final Context context;

    public CurrencyStorageImpl(CurrencyDatabase database, CurrencyApiService currencyApiService, FlagApiService flagApiService, Context context) {
        this.currencyApiService = currencyApiService;
        this.flagApiService = flagApiService;
        this.currencyDao = database.currencyDao();
        this.context = context;
    }

    @Override
    public LiveData<List<Currency>> getDbCurrencies() {
        LiveData<List<Currency>> dbCurrencies = currencyDao.getAllCurrencies();
        if (dbCurrencies.getValue() == null || dbCurrencies.getValue().isEmpty()) {
            fetchCurrencies();
        }
        return dbCurrencies;
    }

    public LiveData<List<Currency>> getApiCurrencies() {
        fetchCurrencies();
        return apiCurrenciesLiveData;
    }

    private void fetchCurrencies() {
        currencyApiService.getExchangeRates().enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<CurrencyApiResponse> call, Response<CurrencyApiResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    List<Currency> currencies = new ArrayList<>();
                    for (Map.Entry<String, Double> entry : response.body().getRates().entrySet()) {
                        String code = entry.getKey();
                        double rate = 1 / entry.getValue();

                        String name = getCurrencyNameFromJson(code);
                        String description = getCurrencyDescriptionFromJson(code);
                        String countryCode = getCurrencyCountryCodeFromJson(code);

                        flagApiService.getFlagUrl(countryCode).enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful() && response.body() != null) {
                                    String flagUrl = "https://flagsapi.com/" + countryCode + "/flat/64.png";
                                    Currency currency = new Currency(code, rate, name, description, flagUrl);
                                    currencies.add(currency);
                                    executorService.execute(() -> currencyDao.insertCurrency(currency));
                                } else {
                                    String flagUrl = "https://flagsapi.com/BE/flat/64.png";
                                    Currency currency = new Currency(code, rate, name, description, flagUrl);
                                    currencies.add(currency);
                                    executorService.execute(() -> currencyDao.insertCurrency(currency));
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.e("Flag API Failure", "Ошибка при получении флага: " + t.getMessage());
                                String flagUrl = "https://flagsapi.com/BE/flat/64.png";
                                Currency currency = new Currency(code, rate, name, description, flagUrl);
                                currencies.add(currency);
                                executorService.execute(() -> currencyDao.insertCurrency(currency));
                            }
                        });
                    }
                    apiCurrenciesLiveData.postValue(currencies);
                } else {
                    Log.e("API Error", "Response error: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<CurrencyApiResponse> call, Throwable t) {
                Log.e("API Failure", "Ошибка при попытке достать валюты из API: " + t.getMessage());
                apiCurrenciesLiveData.setValue(null);
            }
        });
    }


    private String getCurrencyNameFromJson(String code) {
        return getCurrencyJsonProperty(code, "name");
    }

    private String getCurrencyDescriptionFromJson(String code) {
        return getCurrencyJsonProperty(code, "description");
    }

    private String getCurrencyCountryCodeFromJson(String code) {
        return getCurrencyJsonProperty(code, "countryCode");
    }

    private String getCurrencyJsonProperty(String code, String property) {
        try {

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(context.getAssets().open("currencies.json"))
            );
            StringBuilder jsonBuilder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                jsonBuilder.append(line);
            }
            String json = jsonBuilder.toString();

            Gson gson = new Gson();
            Map<String, Map<String, String>> currencies = gson.fromJson(json, Map.class);

            if (currencies.containsKey(code)) {
                return currencies.get(code).get(property);
            } else {
                return "Неизвестная информация";
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "Неизвестная информация";
        }
    }

    @Override
    public LiveData<Currency> getCurrencyLiveData(String code) {
        MutableLiveData<Currency> currencyLiveData = new MutableLiveData<>();
        executorService.execute(() -> {
            Currency currency = currencyDao.getCurrency(code);
            currencyLiveData.postValue(currency);
        });
        return currencyLiveData;
    }
}