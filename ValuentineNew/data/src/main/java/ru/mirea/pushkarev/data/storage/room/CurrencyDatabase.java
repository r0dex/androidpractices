package ru.mirea.pushkarev.data.storage.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import ru.mirea.pushkarev.data.storage.models.Currency;

@Database(entities = {Currency.class}, version = 3)
public abstract class CurrencyDatabase extends RoomDatabase {

    private static CurrencyDatabase instance;

    public abstract CurrencyDao currencyDao();

    public static synchronized CurrencyDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                            CurrencyDatabase.class, "currency_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}