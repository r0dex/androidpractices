package ru.mirea.pushkarev.data.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import java.util.ArrayList;
import java.util.List;

import ru.mirea.pushkarev.data.storage.CurrencyStorage;
import ru.mirea.pushkarev.domain.models.Currency;
import ru.mirea.pushkarev.domain.repository.CurrencyRepository;

public class CurrencyRepositoryImpl implements CurrencyRepository {
    private final CurrencyStorage currencyStorage;
    private final MediatorLiveData<List<Currency>> currenciesLiveData = new MediatorLiveData<>();

    public CurrencyRepositoryImpl(CurrencyStorage currencyStorage) {
        this.currencyStorage = currencyStorage;

        LiveData<List<ru.mirea.pushkarev.data.storage.models.Currency>> dbCurrencies = currencyStorage.getDbCurrencies();

        Observer<List<ru.mirea.pushkarev.data.storage.models.Currency>> dbUpdateObserver = storedCurrencyList -> {
            if (storedCurrencyList != null && !storedCurrencyList.isEmpty()) {
                List<Currency> domainCurrencies = convertToDomainList(storedCurrencyList);
                currenciesLiveData.setValue(domainCurrencies);
            } else {
                fetchApiData();
            }
        };

        currenciesLiveData.addSource(dbCurrencies, dbUpdateObserver);
    }

    private void fetchApiData() {
        LiveData<List<ru.mirea.pushkarev.data.storage.models.Currency>> apiCurrencies = currencyStorage.getApiCurrencies();

        Observer<List<ru.mirea.pushkarev.data.storage.models.Currency>> apiUpdateObserver = storedCurrencyList -> {
            if (storedCurrencyList != null) {
                List<Currency> domainCurrencies = convertToDomainList(storedCurrencyList);
                currenciesLiveData.setValue(domainCurrencies);
            }
        };

        currenciesLiveData.addSource(apiCurrencies, apiUpdateObserver);
    }

    @Override
    public LiveData<List<Currency>> getCurrencyList() {
        return currenciesLiveData;
    }

    @Override
    public LiveData<Currency> getCurrencyDetailsLiveData(String code) {
        MediatorLiveData<Currency> currencyLiveData = new MediatorLiveData<>();

        currencyLiveData.addSource(currencyStorage.getCurrencyLiveData(code), storedCurrency -> {
            if (storedCurrency != null) {
                currencyLiveData.setValue(convertToDomainModel(storedCurrency));
            }
        });

        return currencyLiveData;
    }

    private List<Currency> convertToDomainList(List<ru.mirea.pushkarev.data.storage.models.Currency> storedCurrencyList) {
        List<Currency> domainCurrencies = new ArrayList<>();
        for (ru.mirea.pushkarev.data.storage.models.Currency storedCurrency : storedCurrencyList) {
            domainCurrencies.add(convertToDomainModel(storedCurrency));
        }
        return domainCurrencies;
    }

    private Currency convertToDomainModel(ru.mirea.pushkarev.data.storage.models.Currency storedCurrency) {
        return new Currency(storedCurrency.getCode(), storedCurrency.getName(), storedCurrency.getRate(), storedCurrency.getDescription(), storedCurrency.getFlagUrl());
    }
}
