package ru.mirea.pushkarev.data.settings;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatDelegate;

import ru.mirea.pushkarev.domain.repository.AppSettings;

public class AppSettingsImpl implements AppSettings {

    private static final String PREFS_NAME = "app_settings";
    private static final String THEME_KEY = "theme_key";
    private final SharedPreferences sharedPreferences;

    public AppSettingsImpl(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public void toggleColorTheme() {
        boolean isNightMode = (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES);
        AppCompatDelegate.setDefaultNightMode(isNightMode ? AppCompatDelegate.MODE_NIGHT_NO : AppCompatDelegate.MODE_NIGHT_YES);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(THEME_KEY, !isNightMode);
        editor.apply();
    }
}
