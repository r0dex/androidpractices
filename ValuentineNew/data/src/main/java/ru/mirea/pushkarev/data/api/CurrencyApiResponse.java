package ru.mirea.pushkarev.data.api;

import java.util.Map;

public class CurrencyApiResponse {
    private Map<String, Double> rates;

    public Map<String, Double> getRates() {
        return rates;
    }

}
