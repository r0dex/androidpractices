package ru.mirea.pushkarev.data.storage.sharedprefs;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefsUserStorage {

    private static final String PREFS_NAME = "user_prefs";
    private SharedPreferences sharedPreferences;

    public SharedPrefsUserStorage(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public void saveUserEmail(String email) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_email", email);
        editor.apply();
    }

    public String getUserEmail() {
        return sharedPreferences.getString("user_email", null);
    }

    public void clearUserData() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
