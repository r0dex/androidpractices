package ru.mirea.pushkarev.domain.repository;

public interface AppSettings {
    void toggleColorTheme();
}

