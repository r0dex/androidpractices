package ru.mirea.pushkarev.domain.usecases;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;

import ru.mirea.pushkarev.domain.repository.UserRepository;


public class RegisterUserUseCase {
    private final UserRepository userRepository;

    public RegisterUserUseCase(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Task<FirebaseUser> execute(String email, String password) {
        return userRepository.signUp(email, password);
    }
}
