package ru.mirea.pushkarev.domain.usecases;

import com.google.firebase.auth.FirebaseUser;

import ru.mirea.pushkarev.domain.repository.UserRepository;

public class GetUserProfileUseCase {
    private final UserRepository userRepository;

    public GetUserProfileUseCase(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public FirebaseUser execute() {
        return userRepository.getCurrentUser();
    }
}
