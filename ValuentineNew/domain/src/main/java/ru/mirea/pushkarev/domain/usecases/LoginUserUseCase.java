package ru.mirea.pushkarev.domain.usecases;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;

import ru.mirea.pushkarev.domain.repository.UserRepository;

public class LoginUserUseCase {
    private final UserRepository userRepository;

    public LoginUserUseCase(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Task<FirebaseUser> execute(String email, String password) {
        return userRepository.signIn(email, password);
    }
}
