package ru.mirea.pushkarev.domain.models;

public class Currency {
    private String code;
    private String name;
    private double rate;
    private String description;
    private String flagUrl;

    public Currency(String code, String name, double rate, String description, String flagUrl) {
        this.code = code;
        this.name = name;
        this.rate = rate;
        this.description = description;
        this.flagUrl = flagUrl;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public double getRate() {
        return rate;
    }

    public String getDescription() {
        return description;
    }

    public String getFlagUrl() {  // Геттер для флага
        return flagUrl;
    }
}
