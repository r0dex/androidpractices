package ru.mirea.pushkarev.domain.usecases;

import ru.mirea.pushkarev.domain.repository.UserRepository;

public class LogoutUserUseCase {
    private final UserRepository userRepository;

    public LogoutUserUseCase(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void execute() {
        userRepository.signOut();
    }
}
