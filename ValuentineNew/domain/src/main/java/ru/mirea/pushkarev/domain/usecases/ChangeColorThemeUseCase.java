package ru.mirea.pushkarev.domain.usecases;

import ru.mirea.pushkarev.domain.repository.AppSettings;


public class ChangeColorThemeUseCase {
    private final AppSettings appSettings;


    public ChangeColorThemeUseCase(AppSettings appSettings) {
        this.appSettings = appSettings;
    }

    public void execute() {
        appSettings.toggleColorTheme();
    }
}
