package ru.mirea.pushkarev.domain.repository;

import androidx.lifecycle.LiveData;

import java.util.List;
import ru.mirea.pushkarev.domain.models.Currency;

public interface CurrencyRepository {
    LiveData<List<Currency>> getCurrencyList();


    LiveData<Currency> getCurrencyDetailsLiveData(String code);
}

