package ru.mirea.pushkarev.domain.usecases;

import androidx.lifecycle.LiveData;
import ru.mirea.pushkarev.domain.models.Currency;
import ru.mirea.pushkarev.domain.repository.CurrencyRepository;

import java.util.List;

public class GetCurrencyListUseCase {
    private final CurrencyRepository currencyRepository;

    public GetCurrencyListUseCase(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    public LiveData<List<Currency>> execute() {
        return currencyRepository.getCurrencyList();
    }
}
