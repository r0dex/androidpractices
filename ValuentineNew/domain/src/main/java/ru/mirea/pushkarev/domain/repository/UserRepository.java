package ru.mirea.pushkarev.domain.repository;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;

public interface UserRepository {
    Task<FirebaseUser> signUp(String email, String password);
    Task<FirebaseUser> signIn(String email, String password);
    void signOut();
    FirebaseUser getCurrentUser();
}
