package ru.mirea.pushkarev.valuentine_new.presentation.ui.profile;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseUser;

import ru.mirea.pushkarev.domain.usecases.GetUserProfileUseCase;
import ru.mirea.pushkarev.domain.usecases.LogoutUserUseCase;

public class ProfileViewModel extends ViewModel {
    private final MutableLiveData<FirebaseUser> userProfile = new MutableLiveData<>();
    private final LogoutUserUseCase logoutUserUseCase;

    public ProfileViewModel(GetUserProfileUseCase getUserProfileUseCase, LogoutUserUseCase logoutUserUseCase) {
        this.logoutUserUseCase = logoutUserUseCase;
        FirebaseUser user = getUserProfileUseCase.execute();
        userProfile.setValue(user);
    }

    public LiveData<FirebaseUser> getUserProfile() {
        return userProfile;
    }

    public void logout() {
        logoutUserUseCase.execute();
    }
}
