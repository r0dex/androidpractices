package ru.mirea.pushkarev.valuentine_new;

import android.app.Application;
import android.content.Context;

public class ValuentineNew extends Application {
    private static ValuentineNew instance;

    public static Context getAppContext() {
        return instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
