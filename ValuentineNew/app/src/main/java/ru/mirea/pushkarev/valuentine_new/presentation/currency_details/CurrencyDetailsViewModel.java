package ru.mirea.pushkarev.valuentine_new.presentation.currency_details;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import ru.mirea.pushkarev.domain.models.Currency;
import ru.mirea.pushkarev.domain.usecases.GetCurrencyDetailsUseCase;

public class CurrencyDetailsViewModel extends ViewModel {
    private final GetCurrencyDetailsUseCase getCurrencyDetailsUseCase;
    private LiveData<Currency> currencyDetailsLiveData;
    private final MutableLiveData<Boolean> isLoading = new MutableLiveData<>(false);
    private final MutableLiveData<String> errorMessage = new MutableLiveData<>();

    public CurrencyDetailsViewModel(GetCurrencyDetailsUseCase getCurrencyDetailsUseCase) {
        this.getCurrencyDetailsUseCase = getCurrencyDetailsUseCase;
    }

    public LiveData<Currency> getCurrencyDetailsLiveData(String currencyCode) {
        if (currencyDetailsLiveData == null) {
            isLoading.setValue(true);
            currencyDetailsLiveData = getCurrencyDetailsUseCase.execute(currencyCode);
            isLoading.setValue(false);
        }
        return currencyDetailsLiveData;
    }

    public LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public LiveData<String> getErrorMessage() {
        return errorMessage;
    }
}
