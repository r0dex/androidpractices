package ru.mirea.pushkarev.valuentine_new.presentation.registration;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.pushkarev.data.repository.UserRepositoryImpl;
import ru.mirea.pushkarev.domain.usecases.RegisterUserUseCase;
import ru.mirea.pushkarev.valuentine_new.R;
import ru.mirea.pushkarev.valuentine_new.presentation.MainActivity;
import ru.mirea.pushkarev.valuentine_new.presentation.login.LoginActivity;

public class RegistrationActivity extends AppCompatActivity {
    private EditText emailInput, passwordInput;
    private Button registerButton;
    private TextView loginLink;
    private RegistrationViewModel registrationViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        emailInput = findViewById(R.id.email_input);
        passwordInput = findViewById(R.id.password_input);
        registerButton = findViewById(R.id.register_button);
        loginLink = findViewById(R.id.login_link);

        UserRepositoryImpl userRepository = new UserRepositoryImpl();
        RegisterUserUseCase registerUserUseCase = new RegisterUserUseCase(userRepository);
        registrationViewModel = new ViewModelProvider(this, new RegistrationViewModelFactory(registerUserUseCase)).get(RegistrationViewModel.class);

        registerButton.setOnClickListener(view -> {
            String email = emailInput.getText().toString();
            String password = passwordInput.getText().toString();
            registrationViewModel.register(email, password);
        });

        loginLink.setOnClickListener(view -> {
            startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
            finish();
        });

        registrationViewModel.getRegisteredUserLiveData().observe(this, user -> {
            if (user != null) {
                Toast.makeText(this, "Регистрация успешна: " + user.getEmail(), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
                finish();
            } else {
                Toast.makeText(this, "Ошибка регистрации", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
