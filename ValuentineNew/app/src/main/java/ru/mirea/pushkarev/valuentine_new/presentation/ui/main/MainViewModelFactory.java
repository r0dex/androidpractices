package ru.mirea.pushkarev.valuentine_new.presentation.ui.main;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.pushkarev.domain.usecases.GetCurrencyListUseCase;

public class MainViewModelFactory implements ViewModelProvider.Factory {
    private final GetCurrencyListUseCase getCurrencyListUseCase;

    public MainViewModelFactory(GetCurrencyListUseCase getCurrencyListUseCase) {
        this.getCurrencyListUseCase = getCurrencyListUseCase;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MainViewModel.class)) {
            return (T) new MainViewModel(getCurrencyListUseCase);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}