package ru.mirea.pushkarev.valuentine_new.presentation.login;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseUser;

import ru.mirea.pushkarev.domain.usecases.LoginUserUseCase;

public class LoginViewModel extends ViewModel {
    private final LoginUserUseCase loginUserUseCase;
    private MutableLiveData<FirebaseUser> loggedInUserLiveData = new MutableLiveData<>();

    public LoginViewModel(LoginUserUseCase loginUserUseCase) {
        this.loginUserUseCase = loginUserUseCase;
    }

    public MutableLiveData<FirebaseUser> getLoggedInUserLiveData() {
        return loggedInUserLiveData;
    }

    public void login(String email, String password) {
        loginUserUseCase.execute(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                loggedInUserLiveData.setValue(task.getResult());
            } else {
                loggedInUserLiveData.setValue(null);
            }
        });
    }
}
