package ru.mirea.pushkarev.valuentine_new.presentation.login;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.pushkarev.data.repository.UserRepositoryImpl;
import ru.mirea.pushkarev.domain.repository.UserRepository;
import ru.mirea.pushkarev.domain.usecases.LoginUserUseCase;

public class LoginViewModelFactory implements ViewModelProvider.Factory {
    private final LoginUserUseCase loginUserUseCase;

    public LoginViewModelFactory(Context context) {
        UserRepository userRepository = new UserRepositoryImpl();
        this.loginUserUseCase = new LoginUserUseCase(userRepository);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(loginUserUseCase);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
