package ru.mirea.pushkarev.valuentine_new.presentation.ui.settings;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.pushkarev.data.settings.AppSettingsImpl;
import ru.mirea.pushkarev.domain.usecases.ChangeColorThemeUseCase;

public class SettingsViewModelFactory implements ViewModelProvider.Factory {

    private final Context context;

    public SettingsViewModelFactory(Context context) {
        this.context = context.getApplicationContext();
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SettingsViewModel.class)) {
            AppSettingsImpl appSettings = new AppSettingsImpl(context);
            ChangeColorThemeUseCase changeColorThemeUseCase = new ChangeColorThemeUseCase(appSettings);
            return (T) new SettingsViewModel(changeColorThemeUseCase);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
