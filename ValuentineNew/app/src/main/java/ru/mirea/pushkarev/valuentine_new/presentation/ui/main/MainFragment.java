package ru.mirea.pushkarev.valuentine_new.presentation.ui.main;

import static androidx.core.app.ActivityCompat.recreate;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import ru.mirea.pushkarev.data.api.CurrencyApiService;
import ru.mirea.pushkarev.data.api.FlagApiService;
import ru.mirea.pushkarev.data.api.RetrofitClient;
import ru.mirea.pushkarev.data.repository.CurrencyRepositoryImpl;
import ru.mirea.pushkarev.data.repository.UserRepositoryImpl;
import ru.mirea.pushkarev.data.storage.impl_temp.CurrencyStorageImpl;
import ru.mirea.pushkarev.data.storage.room.CurrencyDatabase;
import ru.mirea.pushkarev.domain.models.Currency;
import ru.mirea.pushkarev.domain.usecases.GetCurrencyListUseCase;
import ru.mirea.pushkarev.domain.usecases.GetUserProfileUseCase;
import ru.mirea.pushkarev.valuentine_new.R;
import ru.mirea.pushkarev.valuentine_new.presentation.login.LoginActivity;

import android.widget.SearchView;

public class MainFragment extends Fragment {

    private RecyclerView recyclerView;
    private MainViewModel viewModel;
    private SearchView searchView;
    private CurrencyAdapter adapter;
    private List<Currency> fullCurrencyList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        recyclerView = view.findViewById(R.id.currency_list);
        searchView = view.findViewById(R.id.search_view);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getContext() == null) return;

        Intent intent = requireActivity().getIntent();
        boolean isGuest = intent.getBooleanExtra("is_guest", false);

        if (!isGuest) {
            GetUserProfileUseCase getUserProfileUseCase = new GetUserProfileUseCase(new UserRepositoryImpl());
            FirebaseUser currentUser = getUserProfileUseCase.execute();

            if (currentUser == null) {
                Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
                startActivity(loginIntent);
                getActivity().finish();
                return;
            }
        }

        initializeViewModel();
        initializeRecyclerView();
        observeCurrencyList();
        setupSearchView();
    }

    private void initializeViewModel() {
        CurrencyDatabase database = CurrencyDatabase.getInstance(getContext());
        CurrencyApiService currencyApiService = RetrofitClient.getCurrencyApiService();
        FlagApiService flagApiService = RetrofitClient.getFlagApiService();

        CurrencyStorageImpl currencyStorage = new CurrencyStorageImpl(database, currencyApiService, flagApiService, requireContext());
        CurrencyRepositoryImpl currencyRepository = new CurrencyRepositoryImpl(currencyStorage);
        GetCurrencyListUseCase getCurrencyListUseCase = new GetCurrencyListUseCase(currencyRepository);

        viewModel = new ViewModelProvider(this, new MainViewModelFactory(getCurrencyListUseCase)).get(MainViewModel.class);
    }

    private void initializeRecyclerView() {
        adapter = new CurrencyAdapter(currency -> {
            Bundle result = new Bundle();
            result.putString("currency_code", currency.getCode());

            NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
            navController.navigate(R.id.action_mainFragment_to_currencyDetailsFragment, result);
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    private void observeCurrencyList() {
        viewModel.getCurrencyList().observe(getViewLifecycleOwner(), currencies -> {
            if (currencies != null) {
                fullCurrencyList = currencies;
                adapter.submitList(currencies);
            }
        });
    }

    private void setupSearchView() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                filterCurrencies(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filterCurrencies(newText);
                return false;
            }
        });
    }

    private void filterCurrencies(String query) {
        if (fullCurrencyList == null || fullCurrencyList.isEmpty()) {
            return;
        }

        List<Currency> filteredList = new ArrayList<>();
        query = query.toLowerCase();

        for (Currency currency : fullCurrencyList) {
            String name = currency.getName().toLowerCase();
            String code = currency.getCode().toLowerCase();

            if (!name.contains("неизвестная информация") &&
                    (name.contains(query) || code.contains(query))) {
                filteredList.add(currency);
            }
        }

        adapter.submitList(filteredList);
    }


    @Override
    public void onResume() {
        super.onResume();
        observeCurrencyList();
    }
}