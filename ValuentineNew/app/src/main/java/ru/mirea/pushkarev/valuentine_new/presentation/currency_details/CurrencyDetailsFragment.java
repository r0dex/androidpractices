package ru.mirea.pushkarev.valuentine_new.presentation.currency_details;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.squareup.picasso.Picasso;

import ru.mirea.pushkarev.data.api.CurrencyApiService;
import ru.mirea.pushkarev.data.api.FlagApiService;
import ru.mirea.pushkarev.data.api.RetrofitClient;
import ru.mirea.pushkarev.data.repository.CurrencyRepositoryImpl;
import ru.mirea.pushkarev.data.storage.impl_temp.CurrencyStorageImpl;
import ru.mirea.pushkarev.data.storage.room.CurrencyDatabase;
import ru.mirea.pushkarev.domain.usecases.GetCurrencyDetailsUseCase;
import ru.mirea.pushkarev.valuentine_new.R;

public class CurrencyDetailsFragment extends Fragment {

    private TextView currencyCodeTextView;
    private TextView currencyNameTextView;
    private TextView currencyRateTextView;
    private TextView currencyDescriptionTextView;
    private ProgressBar progressBar;
    private ImageView flagImageView;

    private CurrencyDetailsViewModel viewModel;

    public CurrencyDetailsFragment() {
        super(R.layout.fragment_currency_details);
    }

    // В CurrencyDetailsFragment, в onViewCreated:
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        currencyCodeTextView = view.findViewById(R.id.currency_code);
        currencyNameTextView = view.findViewById(R.id.currency_name);
        currencyRateTextView = view.findViewById(R.id.currency_rate);
        currencyDescriptionTextView = view.findViewById(R.id.currency_description);
        progressBar = view.findViewById(R.id.progress_bar);
        flagImageView = view.findViewById(R.id.flag_image);

        Bundle args = getArguments();
        if (args != null) {
            String currencyCode = args.getString("currency_code");
            Log.d("kwuh", "фрагмент получил: " + currencyCode);

            if (currencyCode != null) {
                setupViewModel(currencyCode);
            }
        }
    }


    private void setupViewModel(String currencyCode) {
        CurrencyDatabase database = CurrencyDatabase.getInstance(requireContext());
        CurrencyApiService currencyApiService = RetrofitClient.getCurrencyApiService();
        FlagApiService flagApiService = RetrofitClient.getFlagApiService();

        CurrencyStorageImpl currencyStorage = new CurrencyStorageImpl(database, currencyApiService, flagApiService, requireContext());
        CurrencyRepositoryImpl currencyRepository = new CurrencyRepositoryImpl(currencyStorage);
        GetCurrencyDetailsUseCase getCurrencyDetailsUseCase = new GetCurrencyDetailsUseCase(currencyRepository);

        CurrencyDetailsViewModelFactory factory = new CurrencyDetailsViewModelFactory(getCurrencyDetailsUseCase);
        viewModel = new ViewModelProvider(this, factory).get(CurrencyDetailsViewModel.class);

        viewModel.getCurrencyDetailsLiveData(currencyCode).observe(getViewLifecycleOwner(), currency -> {
            if (currency != null) {
                currencyCodeTextView.setText(currency.getCode());
                currencyNameTextView.setText(currency.getName());
                currencyRateTextView.setText(String.valueOf(currency.getRate()));
                currencyDescriptionTextView.setText(currency.getDescription());
                Log.d("kwuh", "Данные вроде как есть " + currencyNameTextView);

                String flagUrl = currency.getFlagUrl();
                Picasso.get().load(flagUrl).into(flagImageView);
            }
        });

        viewModel.getIsLoading().observe(getViewLifecycleOwner(), isLoading -> {
            progressBar.setVisibility(isLoading ? ProgressBar.VISIBLE : ProgressBar.INVISIBLE);
        });

        viewModel.getErrorMessage().observe(getViewLifecycleOwner(), error -> {
            if (error != null) {
                Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show();
            }
        });
    }
}
