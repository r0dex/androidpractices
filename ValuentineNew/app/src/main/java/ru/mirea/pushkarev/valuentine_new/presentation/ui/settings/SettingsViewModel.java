package ru.mirea.pushkarev.valuentine_new.presentation.ui.settings;

import androidx.lifecycle.ViewModel;

import ru.mirea.pushkarev.domain.usecases.ChangeColorThemeUseCase;

public class SettingsViewModel extends ViewModel {

    private final ChangeColorThemeUseCase changeColorThemeUseCase;

    public SettingsViewModel(ChangeColorThemeUseCase changeColorThemeUseCase) {
        this.changeColorThemeUseCase = changeColorThemeUseCase;
    }

    public void changeTheme() {
        changeColorThemeUseCase.execute();
    }
}
