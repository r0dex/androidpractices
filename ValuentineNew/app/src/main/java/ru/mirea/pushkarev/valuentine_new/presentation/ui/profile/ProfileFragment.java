package ru.mirea.pushkarev.valuentine_new.presentation.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;

import ru.mirea.pushkarev.valuentine_new.R;
import ru.mirea.pushkarev.valuentine_new.presentation.login.LoginActivity;

public class ProfileFragment extends Fragment {

    private ProfileViewModel viewModel;
    private TextView userNameTextView;
    private ImageView userImageView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ProfileViewModelFactory factory = new ProfileViewModelFactory();
        viewModel = new ViewModelProvider(this, factory).get(ProfileViewModel.class);

        userNameTextView = view.findViewById(R.id.profile_name);
        userImageView = view.findViewById(R.id.profile_image);
        Button logoutButton = view.findViewById(R.id.logout_button);

        viewModel.getUserProfile().observe(getViewLifecycleOwner(), user -> {
            if (user != null) {
                userNameTextView.setText(user.getEmail());

                if (user.getPhotoUrl() != null) {
                    Glide.with(this).load(user.getPhotoUrl()).into(userImageView);
                }
            }
        });

        logoutButton.setOnClickListener(v -> {
            viewModel.logout();
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
            requireActivity().finish();
        });
    }
}
