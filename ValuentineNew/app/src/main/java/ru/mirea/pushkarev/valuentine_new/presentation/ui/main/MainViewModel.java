package ru.mirea.pushkarev.valuentine_new.presentation.ui.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import ru.mirea.pushkarev.domain.models.Currency;
import ru.mirea.pushkarev.domain.usecases.GetCurrencyListUseCase;

public class MainViewModel extends ViewModel {


    private final LiveData<List<Currency>> currencyList;

    public MainViewModel(GetCurrencyListUseCase getCurrencyListUseCase) {
        currencyList = getCurrencyListUseCase.execute();
    }

    public LiveData<List<Currency>> getCurrencyList() {
        return currencyList;
    }
}