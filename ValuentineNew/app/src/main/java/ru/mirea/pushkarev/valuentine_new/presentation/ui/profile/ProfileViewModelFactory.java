package ru.mirea.pushkarev.valuentine_new.presentation.ui.profile;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.pushkarev.data.repository.UserRepositoryImpl;
import ru.mirea.pushkarev.domain.usecases.GetUserProfileUseCase;
import ru.mirea.pushkarev.domain.usecases.LogoutUserUseCase;

public class ProfileViewModelFactory implements ViewModelProvider.Factory {
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(ProfileViewModel.class)) {
            UserRepositoryImpl userRepository = new UserRepositoryImpl();
            GetUserProfileUseCase getUserProfileUseCase = new GetUserProfileUseCase(userRepository);
            LogoutUserUseCase logoutUserUseCase = new LogoutUserUseCase(userRepository);
            return (T) new ProfileViewModel(getUserProfileUseCase, logoutUserUseCase);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
