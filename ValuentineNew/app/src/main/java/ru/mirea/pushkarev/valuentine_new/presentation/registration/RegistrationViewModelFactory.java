package ru.mirea.pushkarev.valuentine_new.presentation.registration;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.pushkarev.domain.usecases.RegisterUserUseCase;

public class RegistrationViewModelFactory implements ViewModelProvider.Factory {
    private final RegisterUserUseCase registerUserUseCase;

    public RegistrationViewModelFactory(RegisterUserUseCase registerUserUseCase) {
        this.registerUserUseCase = registerUserUseCase;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(RegistrationViewModel.class)) {
            return (T) new RegistrationViewModel(registerUserUseCase);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
