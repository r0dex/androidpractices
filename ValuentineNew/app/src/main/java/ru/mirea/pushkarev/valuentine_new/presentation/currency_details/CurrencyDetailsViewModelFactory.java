package ru.mirea.pushkarev.valuentine_new.presentation.currency_details;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.pushkarev.domain.usecases.GetCurrencyDetailsUseCase;

public class CurrencyDetailsViewModelFactory implements ViewModelProvider.Factory {
    private final GetCurrencyDetailsUseCase getCurrencyDetailsUseCase;

    public CurrencyDetailsViewModelFactory(GetCurrencyDetailsUseCase getCurrencyDetailsUseCase) {
        this.getCurrencyDetailsUseCase = getCurrencyDetailsUseCase;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new CurrencyDetailsViewModel(getCurrencyDetailsUseCase);
    }
}
