package ru.mirea.pushkarev.valuentine_new.presentation.registration;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.auth.FirebaseUser;
import com.google.android.gms.tasks.Task;

import ru.mirea.pushkarev.domain.usecases.RegisterUserUseCase;

public class RegistrationViewModel extends ViewModel {
    private final RegisterUserUseCase registerUserUseCase;
    private MutableLiveData<FirebaseUser> registeredUserLiveData = new MutableLiveData<>();

    public RegistrationViewModel(RegisterUserUseCase registerUserUseCase) {
        this.registerUserUseCase = registerUserUseCase;
    }

    public MutableLiveData<FirebaseUser> getRegisteredUserLiveData() {
        return registeredUserLiveData;
    }

    public void register(String email, String password) {
        registerUserUseCase.execute(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                FirebaseUser user = task.getResult();
                registeredUserLiveData.setValue(user);
            } else {
                registeredUserLiveData.setValue(null);
            }
        });
    }
}
