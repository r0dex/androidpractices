package ru.mirea.pushkarev.valuentine_new.presentation.ui.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.pushkarev.valuentine_new.R;

public class SettingsFragment extends Fragment {

    private SettingsViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SettingsViewModelFactory factory = new SettingsViewModelFactory(requireContext());
        viewModel = new ViewModelProvider(this, factory).get(SettingsViewModel.class);

        Button changeThemeButton = view.findViewById(R.id.change_theme_button);
        changeThemeButton.setOnClickListener(v -> {
            viewModel.changeTheme();
        });
    }
}
