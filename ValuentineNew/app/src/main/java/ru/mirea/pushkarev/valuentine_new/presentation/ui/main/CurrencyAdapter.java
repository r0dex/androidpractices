package ru.mirea.pushkarev.valuentine_new.presentation.ui.main;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.mirea.pushkarev.domain.models.Currency;
import ru.mirea.pushkarev.valuentine_new.R;

public class CurrencyAdapter extends ListAdapter<Currency, CurrencyAdapter.CurrencyViewHolder> {
    private final OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(Currency currency);
    }

    public CurrencyAdapter(OnItemClickListener listener) {
        super(new DiffUtil.ItemCallback<Currency>() {
            @Override
            public boolean areItemsTheSame(@NonNull Currency oldItem, @NonNull Currency newItem) {
                return oldItem.getCode().equals(newItem.getCode());
            }

            @Override
            public boolean areContentsTheSame(@NonNull Currency oldItem, @NonNull Currency newItem) {
                return oldItem.getRate() == newItem.getRate();
            }
        });
        this.onItemClickListener = listener;
    }

    @NonNull
    @Override
    public CurrencyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_currency, parent, false);
        return new CurrencyViewHolder(view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyViewHolder holder, int position) {
        Currency currency = getItem(position);
        holder.bind(currency);
    }

    public static class CurrencyViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameTextView;

        public CurrencyViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.currency_name);

            itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Currency currency = (Currency) v.getTag();
                    listener.onItemClick(currency);
                }
            });
        }

        @SuppressLint("SetTextI18n")
        public void bind(Currency currency) {
            nameTextView.setText(currency.getName() + " (" + currency.getCode() + ")");
            itemView.setTag(currency);
        }
    }
}
