package ru.mirea.pushkarev.toastapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.text.BreakIterator;

public class MainActivity extends AppCompatActivity {

    EditText text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = findViewById(R.id.editText);
    }

    public void onClickNewActivity(View view) {

        Toast toast = Toast.makeText(getApplicationContext(),
                "СТУДЕНТ No 22 ГРУППА БСБО-10-21 Количество символов - " + text.getText().length(),
                Toast.LENGTH_SHORT);
        toast.show();
    }
}