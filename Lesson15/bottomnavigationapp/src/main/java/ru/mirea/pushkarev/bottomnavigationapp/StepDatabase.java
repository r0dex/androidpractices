package ru.mirea.pushkarev.bottomnavigationapp;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {StepCount.class}, version = 2, exportSchema = false)
public abstract class StepDatabase extends RoomDatabase {

    public abstract StepCountDao stepCountDao();

    private static volatile StepDatabase INSTANCE;

    public static StepDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (StepDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                                    StepDatabase.class, "step_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}