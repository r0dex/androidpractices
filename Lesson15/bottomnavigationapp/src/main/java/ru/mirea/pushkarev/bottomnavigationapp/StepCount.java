package ru.mirea.pushkarev.bottomnavigationapp;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class StepCount {

    @PrimaryKey
    @NonNull
    public String date;
    public int stepCount;

    public StepCount(@NonNull String date, int stepCount) {
        this.date = date;
        this.stepCount = stepCount;
    }
}