package ru.mirea.pushkarev.bottomnavigationapp.ui.home;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;

import ru.mirea.pushkarev.bottomnavigationapp.StepCount;
import ru.mirea.pushkarev.bottomnavigationapp.StepDatabase;

public class StepViewModel extends androidx.lifecycle.ViewModel {

    private final StepDatabase database;
    private final MutableLiveData<Integer> steps = new MutableLiveData<>(0);

    public StepViewModel(Application application) {
        database = StepDatabase.getDatabase(application);
    }

    public LiveData<Integer> getStepsForDate(String date) {
        return database.stepCountDao().getStepsForDate(date);
    }

    public LiveData<List<StepCount>> getAllSteps() {
        return database.stepCountDao().getAllSteps();
    }

    public void resetSteps() {
        String currentDate = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(new Date());
        Executors.newSingleThreadExecutor().execute(() -> {
            database.stepCountDao().resetStepsForDate(currentDate);
        });
    }

    public void incrementSteps() {
        String currentDate = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(new Date());
        Executors.newSingleThreadExecutor().execute(() -> {
            database.stepCountDao().insertOrUpdateStepCount(currentDate);
        });
    }
}
