package ru.mirea.pushkarev.bottomnavigationapp.ui.history;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.mirea.pushkarev.bottomnavigationapp.R;
import ru.mirea.pushkarev.bottomnavigationapp.StepCount;
import ru.mirea.pushkarev.bottomnavigationapp.ui.home.StepAdapter;
import ru.mirea.pushkarev.bottomnavigationapp.ui.home.StepViewModel;



public class HistoryFragment extends Fragment {

    private RecyclerView recyclerView;
    private StepAdapter stepAdapter;
    private StepViewModel stepViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

        stepAdapter = new StepAdapter();
        recyclerView.setAdapter(stepAdapter);

        stepViewModel = new ViewModelProvider(requireActivity()).get(StepViewModel.class);
        stepViewModel.getAllSteps().observe(getViewLifecycleOwner(), stepCounts -> stepAdapter.setSteps(stepCounts));

        return view;
    }
}