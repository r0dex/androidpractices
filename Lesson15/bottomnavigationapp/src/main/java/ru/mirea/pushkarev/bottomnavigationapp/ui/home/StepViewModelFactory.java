package ru.mirea.pushkarev.bottomnavigationapp.ui.home;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class StepViewModelFactory implements ViewModelProvider.Factory {
    private final Application application;

    public StepViewModelFactory(Application application) {
        this.application = application;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(StepViewModel.class)) {
            return (T) new StepViewModel(application);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
