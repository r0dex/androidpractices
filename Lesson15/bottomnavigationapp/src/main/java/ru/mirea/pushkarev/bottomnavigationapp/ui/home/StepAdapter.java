package ru.mirea.pushkarev.bottomnavigationapp.ui.home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

import ru.mirea.pushkarev.bottomnavigationapp.R;
import ru.mirea.pushkarev.bottomnavigationapp.StepCount;

public class StepAdapter extends RecyclerView.Adapter<StepAdapter.StepViewHolder> {

    private List<StepCount> steps = new ArrayList<>();

    public void setSteps(List<StepCount> steps) {
        this.steps = steps;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public StepViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.step_item, parent, false);
        return new StepViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StepViewHolder holder, int position) {
        StepCount step = steps.get(position);
        holder.dateTextView.setText(step.date);
        holder.stepCountTextView.setText(String.valueOf(step.stepCount));
    }

    @Override
    public int getItemCount() {
        return steps.size();
    }

    static class StepViewHolder extends RecyclerView.ViewHolder {

        private final TextView dateTextView;
        private final TextView stepCountTextView;

        public StepViewHolder(@NonNull View itemView) {
            super(itemView);
            dateTextView = itemView.findViewById(R.id.dateTextView);
            stepCountTextView = itemView.findViewById(R.id.stepCountTextView);
        }
    }
}
