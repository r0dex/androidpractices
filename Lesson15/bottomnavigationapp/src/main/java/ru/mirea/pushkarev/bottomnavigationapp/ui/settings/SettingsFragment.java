package ru.mirea.pushkarev.bottomnavigationapp.ui.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.pushkarev.bottomnavigationapp.R;
import ru.mirea.pushkarev.bottomnavigationapp.ui.home.StepViewModel;

public class SettingsFragment extends Fragment {

    private StepViewModel stepViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        Button resetDatabaseButton = view.findViewById(R.id.resetDatabaseButton);

        stepViewModel = new ViewModelProvider(requireActivity()).get(StepViewModel.class);

        resetDatabaseButton.setOnClickListener(v -> {
            stepViewModel.resetSteps();
            Toast.makeText(requireContext(), "Шаги за сегодня сброшены!", Toast.LENGTH_SHORT).show();
        });

        return view;
    }
}