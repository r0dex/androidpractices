package ru.mirea.pushkarev.bottomnavigationapp.ui.home;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.mirea.pushkarev.bottomnavigationapp.R;

public class HomeFragment extends Fragment implements SensorEventListener {

    private TextView stepCounter;
    private TextView dateTextView;
    private SensorManager sensorManager;
    private Sensor accelerometerSensor;
    private boolean isSensorAvailable;
    private StepViewModel stepViewModel;

    private static final float SHAKE_THRESHOLD = 15.0f;
    private float lastX, lastY, lastZ;
    private long lastTime = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        stepCounter = view.findViewById(R.id.stepCounter);
        dateTextView = view.findViewById(R.id.dateTextView);

        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        dateTextView.setText(currentDate);

        sensorManager = (SensorManager) requireActivity().getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager != null) {
            accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }
        isSensorAvailable = accelerometerSensor != null;

        stepViewModel = new ViewModelProvider(requireActivity()).get(StepViewModel.class);

        stepViewModel.getStepsForDate(currentDate).observe(getViewLifecycleOwner(), steps -> {
            if (steps != null) {
                stepCounter.setText(String.valueOf(steps));
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isSensorAvailable) {
            sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_UI);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isSensorAvailable) {
            sensorManager.unregisterListener(this);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            // Получаем значения ускорения по осям X, Y, Z
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            // Вычисляем ускорение
            float deltaX = x - lastX;
            float deltaY = y - lastY;
            float deltaZ = z - lastZ;

            long currentTime = System.currentTimeMillis();
            if (currentTime - lastTime > 100) {  // Ограничиваем частоту обновлений (100 мс)
                lastTime = currentTime;

                // Если сумма изменения ускорений по осям превышает порог, считаем шаг
                if (Math.abs(deltaX) + Math.abs(deltaY) + Math.abs(deltaZ) > SHAKE_THRESHOLD) {
                    Log.d("StepCounter", "Зафиксирована тряска");
                    stepViewModel.incrementSteps();  // Инкрементируем шаги
                }
            }

            // Сохраняем текущие значения для последующего сравнения
            lastX = x;
            lastY = y;
            lastZ = z;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Здесь ничего не нужно делать
    }
}
