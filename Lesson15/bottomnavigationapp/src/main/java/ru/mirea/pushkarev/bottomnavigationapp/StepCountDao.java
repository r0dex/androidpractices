package ru.mirea.pushkarev.bottomnavigationapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface StepCountDao {
    @Query("SELECT stepCount FROM StepCount WHERE date = :date")
    LiveData<Integer> getStepsForDate(String date);

    @Query("SELECT * FROM StepCount")
    LiveData<List<StepCount>> getAllSteps();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(StepCount stepCount);

    @Query("SELECT * FROM StepCount WHERE date = :date LIMIT 1")
    StepCount getStepCountByDate(String date);

    default void insertOrUpdateStepCount(String date) {
        StepCount existingStepCount = getStepCountByDate(date);
        if (existingStepCount == null) {
            insert(new StepCount(date, 1));
        } else {
            existingStepCount.stepCount++;
            insert(existingStepCount);
        }
    }

    @Query("UPDATE StepCount SET stepCount = 0 WHERE date = :date")
    void resetStepsForDate(String date);
}