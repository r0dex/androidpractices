package ru.mirea.pushkarev.navigationdrawerapp.ui.devices;

import androidx.lifecycle.ViewModel;

public class DevicesFragmentViewModel extends ViewModel {
    private String[] devices = {"Galaxy S23", "Galaxy Z Flip 5", "Galaxy Tab S8"};
    private String[] deviceUrls = {
            "https://www.samsung.com/galaxy-s23",
            "https://www.samsung.com/galaxy-z-flip-5",
            "https://www.samsung.com/galaxy-tab-s8"
    };

    public String[] getDevices() {
        return devices;
    }

    public String[] getDeviceUrls() {
        return deviceUrls;
    }
}
