package ru.mirea.pushkarev.navigationdrawerapp.ui.info;

import androidx.lifecycle.ViewModel;

public class InfoFragmentViewModel extends ViewModel {
    private String companyInfo;

    public InfoFragmentViewModel() {
        // Инициализируем данные по умолчанию
        companyInfo = "Samsung is a South Korean multinational conglomerate known for its electronics, mobile devices, and other technological advancements.";
    }

    public String getCompanyInfo() {
        return companyInfo;
    }

    public void setCompanyInfo(String info) {
        this.companyInfo = info;
    }
}