package ru.mirea.pushkarev.navigationdrawerapp.ui;

import androidx.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private String companyInfo = "Samsung is a South Korean multinational conglomerate known for its electronics, mobile devices, and other technological advancements.";

    public String getCompanyInfo() {
        return companyInfo;
    }

    public void setCompanyInfo(String info) {
        this.companyInfo = info;
    }
}