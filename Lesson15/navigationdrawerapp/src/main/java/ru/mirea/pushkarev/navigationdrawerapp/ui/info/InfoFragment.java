package ru.mirea.pushkarev.navigationdrawerapp.ui.info;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;

import ru.mirea.pushkarev.navigationdrawerapp.R;

public class InfoFragment extends Fragment {

    private InfoFragmentViewModel viewModel;

    public InfoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Получаем ViewModel
        viewModel = new ViewModelProvider(this).get(InfoFragmentViewModel.class);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        TextView textInfo = view.findViewById(R.id.textInfo);
        ImageView imageView = view.findViewById(R.id.imageView);

        // Устанавливаем данные
        textInfo.setText(viewModel.getCompanyInfo());
        imageView.setImageResource(R.drawable.samsung_logo);

        return view;
    }
}

