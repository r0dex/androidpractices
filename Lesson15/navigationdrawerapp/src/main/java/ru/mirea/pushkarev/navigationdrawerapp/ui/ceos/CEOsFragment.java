package ru.mirea.pushkarev.navigationdrawerapp.ui.ceos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.pushkarev.navigationdrawerapp.R;

public class CEOsFragment extends Fragment {

    private CEOsFragmentViewModel viewModel;

    public CEOsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Получаем ViewModel
        viewModel = new ViewModelProvider(this).get(CEOsFragmentViewModel.class);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ceos, container, false);

        ListView listView = view.findViewById(R.id.ceosListView);
        CeoAdapter adapter = new CeoAdapter(getActivity(), viewModel.getCeos(), viewModel.getCeoImages());
        listView.setAdapter(adapter);

        return view;
    }

    static class CeoAdapter extends ArrayAdapter<String> {
        private final String[] ceos;
        private final int[] images;

        public CeoAdapter(FragmentActivity context, String[] ceos, int[] images) {
            super(context, R.layout.list_item_ceo, ceos);
            this.ceos = ceos;
            this.images = images;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_ceo, parent, false);
            }

            TextView ceoName = convertView.findViewById(R.id.ceoName);
            ImageView ceoImage = convertView.findViewById(R.id.ceoImage);

            ceoName.setText(ceos[position]);
            ceoImage.setImageResource(images[position]);

            return convertView;
        }
    }
}