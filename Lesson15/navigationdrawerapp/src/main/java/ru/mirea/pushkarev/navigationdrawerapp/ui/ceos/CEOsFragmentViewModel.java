package ru.mirea.pushkarev.navigationdrawerapp.ui.ceos;

import androidx.lifecycle.ViewModel;

import ru.mirea.pushkarev.navigationdrawerapp.R;

public class CEOsFragmentViewModel extends ViewModel {
    private String[] ceos = {"Lee Byung-chul", "Lee Kun-hee", "Lee Jae-yong"};
    private int[] ceoImages = {R.drawable.lee_byung_chul, R.drawable.lee_kun_hee, R.drawable.lee_jae_yong};

    public String[] getCeos() {
        return ceos;
    }

    public int[] getCeoImages() {
        return ceoImages;
    }
}
