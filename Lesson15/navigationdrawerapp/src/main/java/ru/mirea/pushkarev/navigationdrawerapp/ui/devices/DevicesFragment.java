package ru.mirea.pushkarev.navigationdrawerapp.ui.devices;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.pushkarev.navigationdrawerapp.R;

public class DevicesFragment extends Fragment {

    private DevicesFragmentViewModel viewModel;

    public DevicesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Получаем ViewModel
        viewModel = new ViewModelProvider(this).get(DevicesFragmentViewModel.class);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_devices, container, false);

        ListView listView = view.findViewById(R.id.devicesListView);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, viewModel.getDevices());
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((parent, view1, position, id) -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(viewModel.getDeviceUrls()[position]));
            startActivity(browserIntent);
        });

        return view;
    }
}