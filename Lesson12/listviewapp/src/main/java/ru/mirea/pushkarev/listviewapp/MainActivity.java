package ru.mirea.pushkarev.listviewapp;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Book> books = new ArrayList<>();
        books.add(new Book("Джейн Остин", "Гордость и предубеждение"));
        books.add(new Book("Эрих Мария Ремарк", "Три товарища"));
        books.add(new Book("Дэниел Киз", "Цветы для Элджернона"));
        books.add(new Book("Айн Рэнд", "Атлант расправил плечи"));
        books.add(new Book("Фрэнсис Скотт Фицджеральд", "Великий Гэтсби"));
        books.add(new Book("Фёдор Достоевский", "Братья Карамазовы"));
        books.add(new Book("Габриэль Гарсиа Маркес", "Сто лет одиночества"));
        books.add(new Book("Эрнст Хеммингуэй", "Старик и море"));
        books.add(new Book("Шарлотта Бронте", "Джейн Эйр"));
        books.add(new Book("Филип Дик", "Мечтают ли андроиды об электроовцах?"));
        books.add(new Book("Франц Кафка", "Процесс"));
        books.add(new Book("Юрий Бондарев", "Горячий снег"));
        books.add(new Book("Джеймс Дикки", "Избавление"));
        books.add(new Book("Теодор Драйзер", "Американская трагедия"));
        books.add(new Book("Роберт Пенн Уоррен", "Вся королевская рать"));
        books.add(new Book("Курт Воннегут", "Бойня номер пять, или Крестовый поход детей"));
        books.add(new Book("Эмили Бронте", "Грозовой перевал"));
        books.add(new Book("Чарльз Диккенс", "Большие надежды"));
        books.add(new Book("Энтони Бёрджесс", "Заводной апельсин"));
        books.add(new Book("Михаил Веллер", "Приключения майора Звягина"));
        books.add(new Book("Чак Паланик", "Колыбельная"));
        books.add(new Book("Варлам Шаламов", "Колымские рассказы"));
        books.add(new Book("Иван Бунин", "Жизнь Арсеньева"));
        books.add(new Book("Борис Васильев", "В списках не значился"));
        books.add(new Book("Анатолий Кузнецов", "Бабий яр"));
        books.add(new Book("Марк Херман", "Мальчик в полосатой пижаме"));
        books.add(new Book("Кен Кизи", "Пролетая над гнездом кукушки"));
        books.add(new Book("Альбер Камю", "Посторонний"));
        books.add(new Book("Никколо Макиавелли", "Государь"));
        books.add(new Book("Томас Пинчон", "Радуга тяготения"));
        books.add(new Book("Эрих Мария Ремарк", "Триумфальная арка"));

        ListView booksListView = findViewById(R.id.country_list_view);

        ArrayAdapter<Book> adapter = new ArrayAdapter<Book>(this, R.layout.item, books) {
            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                if (convertView == null) {
                    convertView = getLayoutInflater().inflate(R.layout.item, parent, false);
                }

                Book currentBook = getItem(position);

                TextView authorTextView = convertView.findViewById(R.id.author_text_view);
                assert currentBook != null;
                authorTextView.setText(currentBook.author);

                TextView titleTextView = convertView.findViewById(R.id.book_title_text_view);
                titleTextView.setText(currentBook.title);

                return convertView;
            }
        };

        booksListView.setAdapter(adapter);
    }

    static class Book {
        public final String author;
        public final String title;

        public Book(String author, String title) {
            this.author = author;
            this.title = title;
        }
    }
}
