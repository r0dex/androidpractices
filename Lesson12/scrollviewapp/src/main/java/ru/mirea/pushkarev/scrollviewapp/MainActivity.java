package ru.mirea.pushkarev.scrollviewapp;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.math.BigInteger;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        LinearLayout wrapper = findViewById(R.id.wrapper);
        BigInteger base = BigInteger.ONE;
        BigInteger multiplier = BigInteger.valueOf(2);
        for (int i = 0; i < 100; i++) {
            View view = getLayoutInflater().inflate(R.layout.item, wrapper, false);

            TextView numberText = view.findViewById(R.id.number);
            numberText.setText(String.valueOf(i + 1));

            BigInteger value = base.multiply(multiplier.pow(i));
            TextView text = view.findViewById(R.id.textView);
            text.setText(String.format(" %s", value));

            wrapper.addView(view);
        }
    }
}
