package ru.mirea.pushkarev.lesson12.presentation;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.pushkarev.domain.models.Movie;

import ru.mirea.pushkarev.lesson12.R;

public class MainActivity extends AppCompatActivity {

    private MainViewModel vm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vm = new ViewModelProvider(this, new ViewModelFactory(this)).get(MainViewModel.class);

        EditText text = findViewById(R.id.inputFilm);
        TextView textView = findViewById(R.id.showFilm);

        vm.getFavoriteMovie().observe(this, s -> {
            if (vm.isSaveAction()) {
                textView.setText(String.format("Save result: %s", s));
            } else {
                textView.setText(s);
            }
        });

        findViewById(R.id.buttonSaveMovie).setOnClickListener(view -> vm.setText(new Movie(2, text.getText().toString())));

        findViewById(R.id.buttonGetMovie).setOnClickListener(view -> vm.getText());
    }

}
