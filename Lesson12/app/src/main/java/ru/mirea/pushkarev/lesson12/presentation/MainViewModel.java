package ru.mirea.pushkarev.lesson12.presentation;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import ru.mirea.pushkarev.domain.models.Movie;
import ru.mirea.pushkarev.domain.repository.MovieRepository;
import ru.mirea.pushkarev.domain.usecases.SaveMovieToFavoriteUseCase;
import ru.mirea.pushkarev.domain.usecases.GetFavoriteFilmUseCase;

public class MainViewModel extends ViewModel {
    private MovieRepository movieRepository;
    private MutableLiveData<String> favoriteMovie = new MutableLiveData<>();
    private boolean isSaveAction = false;

    public MutableLiveData<String> getFavoriteMovie() {
        return favoriteMovie;
    }

    public MainViewModel(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public void setText(Movie movie) {
        isSaveAction = true;
        Boolean result = new SaveMovieToFavoriteUseCase(movieRepository).execute(movie);
        favoriteMovie.setValue(result.toString());
    }

    public void getText() {
        isSaveAction = false;
        Movie movie = new GetFavoriteFilmUseCase(movieRepository).execute();
        favoriteMovie.setValue(String.format("My favorite movie is %s", movie.getName()));
    }

    public boolean isSaveAction() {
        return isSaveAction;
    }
}
