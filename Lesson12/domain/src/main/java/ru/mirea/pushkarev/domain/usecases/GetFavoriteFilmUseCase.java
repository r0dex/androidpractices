package ru.mirea.pushkarev.domain.usecases;

import ru.mirea.pushkarev.domain.models.Movie;
import ru.mirea.pushkarev.domain.repository.MovieRepository;

public class GetFavoriteFilmUseCase {
    private MovieRepository movieRepository;
    public GetFavoriteFilmUseCase(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }
    public Movie execute(){
        return movieRepository.getMovie();
    }
}
