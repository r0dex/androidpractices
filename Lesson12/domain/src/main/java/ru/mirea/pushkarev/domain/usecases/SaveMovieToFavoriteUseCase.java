package ru.mirea.pushkarev.domain.usecases;

import ru.mirea.pushkarev.domain.models.Movie;
import ru.mirea.pushkarev.domain.repository.MovieRepository;

public class SaveMovieToFavoriteUseCase {
    private MovieRepository movieRepository;
    public SaveMovieToFavoriteUseCase(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }
    public boolean execute(Movie movie){
        return movieRepository.saveMovie(movie);
    }
}
