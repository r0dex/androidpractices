package ru.mirea.pushkarev.domain.repository;

import ru.mirea.pushkarev.domain.models.Movie;

public interface MovieRepository {
    public boolean saveMovie(Movie movie);
    public Movie getMovie();
}