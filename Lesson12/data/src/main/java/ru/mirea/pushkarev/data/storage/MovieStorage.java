package ru.mirea.pushkarev.data.storage;

import ru.mirea.pushkarev.data.storage.models.Movie;

public interface MovieStorage {
    Movie get();
    boolean save(Movie movie);
}
