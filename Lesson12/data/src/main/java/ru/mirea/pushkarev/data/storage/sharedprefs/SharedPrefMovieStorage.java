package ru.mirea.pushkarev.data.storage.sharedprefs;

import android.content.Context;
import android.content.SharedPreferences;

import ru.mirea.pushkarev.data.storage.MovieStorage;
import ru.mirea.pushkarev.data.storage.models.Movie;

public class SharedPrefMovieStorage implements MovieStorage {
    private static final String SHARED_PREFS_NAME = "prefs";
    private static final String KEY_NAME = "movie_name";
    private static final String KEY_ID = "movie_id";

    private SharedPreferences sharedPreferences;

    public SharedPrefMovieStorage(Context context) {
        sharedPreferences = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public Movie get() {
        String movieName = sharedPreferences.getString(KEY_NAME, null);
        int movieId = sharedPreferences.getInt(KEY_ID, -1);
        return new Movie(movieId, movieName);
    }

    @Override
    public boolean save(Movie movie) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_ID, movie.getId());
        editor.putString(KEY_NAME, movie.getName());
        return editor.commit();
    }
}
