package ru.mirea.pushkarev.data.repository;

import ru.mirea.pushkarev.data.storage.models.Movie;
import ru.mirea.pushkarev.data.storage.MovieStorage;
import ru.mirea.pushkarev.domain.repository.MovieRepository;

public class MovieRepositoryImpl implements MovieRepository {
    private MovieStorage movieStorage;

    public MovieRepositoryImpl(MovieStorage movieStorage) {
        this.movieStorage = movieStorage;
    }

    @Override
    public boolean saveMovie(ru.mirea.pushkarev.domain.models.Movie movie) {
        Movie storageMovie =
                new Movie(movie.getId(), movie.getName());
        return movieStorage.save(storageMovie);
    }

    @Override
    public ru.mirea.pushkarev.domain.models.Movie getMovie() {
        Movie storageMovie = movieStorage.get();
        return new ru.mirea.pushkarev.domain.models.Movie(storageMovie.getId(), storageMovie.getName());
    }
}

