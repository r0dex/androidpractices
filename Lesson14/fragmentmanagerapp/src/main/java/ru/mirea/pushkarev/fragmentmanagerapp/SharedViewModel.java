package ru.mirea.pushkarev.fragmentmanagerapp;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SharedViewModel extends ViewModel {
    private final MutableLiveData<String> selectedCountryKey = new MutableLiveData<>();

    public void selectCountry(String countryKey) {
        selectedCountryKey.setValue(countryKey);
    }

    public LiveData<String> getSelectedCountryKey() {
        return selectedCountryKey;
    }
}
