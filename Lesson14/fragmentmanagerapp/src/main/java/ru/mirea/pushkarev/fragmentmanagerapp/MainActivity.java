package ru.mirea.pushkarev.fragmentmanagerapp;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.countriesListFragment, new CountriesListFragment())
                    .commit();
        }

        SharedViewModel viewModel = new ViewModelProvider(this).get(SharedViewModel.class);

        viewModel.getSelectedCountryKey().observe(this, countryKey -> {
            CountriesDetailsFragment detailsFragment = new CountriesDetailsFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.countriesDetailsFragment, detailsFragment)
                    .addToBackStack(null)
                    .commit();
        });
    }
}
