package ru.mirea.pushkarev.fragmentmanagerapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.HashMap;
import java.util.Map;

public class CountriesDetailsFragment extends Fragment {
    private SharedViewModel viewModel;
    private final Map<String, String> countryMap = new HashMap<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_countries_details, container, false);
        TextView detailsTextView = view.findViewById(R.id.detailsTextView);

        initializeCountryMap();

        viewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

        viewModel.getSelectedCountryKey().observe(getViewLifecycleOwner(), countryKey -> {
            String details = countryMap.get(countryKey);
            if (details != null) {
                detailsTextView.setText(details);
            } else {
                detailsTextView.setText("Информация недоступна");
            }
        });

        return view;
    }

    private void initializeCountryMap() {
        countryMap.put("Россия", "Россия - государство в Восточной Европе и Северной Азии. Россия — крупнейшее государство в мире, её территория в международно признанных границах составляет 17 098 246 км²");
        countryMap.put("США", "США - государство в Северной Америке площадью в 9,8 млн км² (3-е место в мире). Соединённые Штаты граничат на севере с Канадой, на юге — с Мексикой, имеют также морскую границу с Россией на западе (по линии перемены дат) и с Кубой на юге. Омываются Тихим океаном с запада, Атлантическим океаном — с востока, и Северным Ледовитым океаном — с севера. Страна отличается, ввиду своих размеров, очень большим разнообразием ландшафтов, климатических зон, растительного и животного мира.");
        countryMap.put("Китай", "Китай - государство в Восточной Азии. С востока страна омывается водами западных морей Тихого океана. На северо-востоке она граничит с КНДР и Россией, на севере — с Монголией, на северо-западе — с Россией и Казахстаном, на западе — с Кыргызстаном, Таджикистаном и Афганистаном, на юго-западе — с контролируемым Пакистаном Гилгит-Балтистаном, Индией, Непалом и Бутаном, на юге — с Мьянмой, Лаосом, Вьетнамом. Общая площадь территории составляет 9,6 млн км².");
        countryMap.put("Германия", "Германия - государство в Центральной Европе со столицей в Берлине. Площадь территории — 357 592 км². Численность населения на январь 2023 года — 84,4 млн человек. Занимает 19-е место в мире по численности населения (1-е место в ЕС, при учёте России и Турции 3-е в Европе) и 62-е в мире по территории (8-е в Европе).");
        countryMap.put("Япония", "Япония - островное государство в Восточной Азии. Находится в Тихом океане к востоку от Японского моря, Китая, Северной и Южной Кореи, России. Занимает территорию от Охотского моря на севере до Восточно-Китайского моря и Тайваня на юге. Поэтическое название — Страна восходящего солнца.");
    }
}
