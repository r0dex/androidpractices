package ru.mirea.pushkarev.resultapifragmentapp;

public interface FragmentListener {
    void sendResult(String message);
}