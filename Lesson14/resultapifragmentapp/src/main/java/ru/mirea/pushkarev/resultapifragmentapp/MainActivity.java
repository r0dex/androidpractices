package ru.mirea.pushkarev.resultapifragmentapp;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements FragmentListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.data_fragment, new DataFragment())
                    .replace(R.id.blank_fragment, new BlankFragment())
                    .commit();
        }

        getSupportFragmentManager().setFragmentResultListener("requestKey", this, (requestKey, bundle) -> {
            String result =  bundle.getString("requestKey");

            if (result != null) {
                Log.d(MainActivity.class.getSimpleName(), "I'm MainActivity, result: " + result);
            } else {
                Log.d(MainActivity.class.getSimpleName(), "Empty result");
            }
        });
    }

    @Override
    public void sendResult(String message) {
        Log.d(MainActivity.class.getSimpleName(), "msg: " + message);
    }
}
