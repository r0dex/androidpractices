package ru.mirea.pushkarev.resultapifragmentapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class BottomSheetFragment extends BottomSheetDialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(BottomSheetFragment.class.getSimpleName(), "BottomSheetFragment onCreate()");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bottom_sheet, container, false);

        getParentFragmentManager().setFragmentResultListener("requestKey", this, (requestKey, bundle) -> {
            String text = bundle.getString("requestKey");
            Log.d(BottomSheetFragment.class.getSimpleName(), "Received text in BottomSheet: " + text);

            TextView textView = view.findViewById(R.id.textView);
            if (text != null && textView != null) {
                textView.setText(text);
            }
        });

        return view;
    }

}
