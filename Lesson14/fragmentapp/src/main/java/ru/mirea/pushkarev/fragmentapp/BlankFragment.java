package ru.mirea.pushkarev.fragmentapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class BlankFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_blank, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        if (getArguments() != null) {
            int numberStudent = getArguments().getInt("my_number_student");
            Log.d(BlankFragment.class.getSimpleName(), "Number: " + numberStudent);

            TextView textView = view.findViewById(R.id.myNumber);
            textView.setText(String.valueOf(numberStudent));
        }
    }
}